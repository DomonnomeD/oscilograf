#include "ssd1963.h"

void SSD_Init()
{
	SSD_Pins_init();

	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RESET);
	TIM7_delay_ms(SSD1963_GlobalDelay);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RD);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);


	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RESET);
	TIM7_delay_ms(SSD1963_GlobalDelay);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RESET);
	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SOFT_RESET);
	TIM7_delay_ms(SSD1963_GlobalDelay); //5ms recomended by datasheet (23)


	SSD_WriteCommand(SSD1963_SET_PLL); //Start PLL
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x01); //Enable PLL, but still use reference clock as system clock
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	TIM7_delay_ms(SSD1963_GlobalDelay); //Wait at least 100us to let the PLL stable
	SSD_WriteCommand(SSD1963_SET_PLL); //Lock PLL
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x03); //Now use PLL output as system clock
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_LCD_MODE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x20); //LCD 24 bit
	SSD_WriteData(0x00); //HSync + VSync + DE MODE
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) >> 8);
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) & 0x00FF);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) >> 8);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) & 0x00FF);
	SSD_WriteData(0x00); //RGB TFT
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_PIXEL_DATA_INTERFACE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x00); //8 bit MCU interface
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_PIXEL_FORMAT);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x70); //24 bit per pixel
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_LSHIFT_FREQ);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x01); //SET PCLK freq=4.94MHz  ; pixel clock frequency (0x02ffff works as well)
	SSD_WriteData(0x45);
	SSD_WriteData(0x47);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_HORI_PERIOD);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x02);			//SET HSYNC Total=600 (display + non-display)
	SSD_WriteData(0x0d);
	SSD_WriteData(0x00);			//SET HPS = 68 (non-display period before the first display data)
	SSD_WriteData(0x2b);
	SSD_WriteData(0x28);			//SET HPW (HSYNC pulse width (LLINE) in pixel clock
	SSD_WriteData(0x00);			//SET LPS (HSYNC pulse start location in pixel clock)
	SSD_WriteData(0x00);
	SSD_WriteData(0x00);			//SET LPSPP (HSYNC pulse subpixel start position)
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_VERT_PERIOD);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x01);			//SET VSYNC Total=360 (display + non-display)
	SSD_WriteData(0x1d);
	SSD_WriteData(0x00);			//SET VPS (non-display period before the first display data)
	SSD_WriteData(0x0c);
	SSD_WriteData(0x09);			//SET VPW (VSYNC pulse width (LFRAME) in lines)
	SSD_WriteData(0x00);			//SET FPS (VSYNC pulse start location in lines)
	SSD_WriteData(0x00);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_ENTER_NORMAL_MODE); //In this case the whole display area will be used
	SSD_WriteCommand(SSD1963_EXIT_IDLE_MODE); //Because in the idle mode only 16 basic colors are used
	//SSD_WriteCommand(SSD1963_SET_DISPLAY_ON);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_TEAR_ON); //TEAR signal is on...
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x00); //... and consist only of V-blanking information
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	//16 bit (565 format)
	SSD_WriteCommand(SSD1963_SET_PIXEL_DATA_INTERFACE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x03); //16 bit LCD interface
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	// Use whole display area
	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0); //First Col
	SSD_WriteData(0);
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) >> 8); //Last Col
	SSD_WriteData(TFT_HORIZONTAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0); //First Page
	SSD_WriteData(0);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) >> 8); //Last Page
	SSD_WriteData(TFT_VERTICAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	///SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	///GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_ADDRESS_MODE); //flip x/y axys places
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0b00100011); //0x00 set as default
												//5 bit pokazivaet kak vivodiatsia izobrazenie\
												//1<<5 - vivodit stolbcami (zapolniaet stolbec, idet k sledujusej)
												//0<<5 - vivodit strokami (zapolniaet stroku, idet k sledujusej)
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	TIM7_delay_ms(SSD1963_GlobalDelay);

	SSD_WriteCommand(SSD1963_SET_DISPLAY_ON);

	//SSD_fillScreen(0x0000000); //Fill with black
}

void SSD_Pins_init( void )
{
	SSD1963_DATA_BUS->MODER = 0x55555555UL; //All outputs
	SSD1963_DATA_BUS->OSPEEDR = 0xFFFFFFFFUL; //All medium speed (50 MHz)

	SSD1963_DATA_BUS->PUPDR |= 0xAAAAAAAAUL; //pull down


	/*for 01 */
//	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_RESET<<SSD1963_SIGNAL_RESET-1;
//	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_CS<<SSD1963_SIGNAL_CS-1;
//	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_DC<<SSD1963_SIGNAL_DC-1;
//	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_RD<<SSD1963_SIGNAL_RD-1;
//	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_WR<<SSD1963_SIGNAL_WR-1;
//
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_RESET<<SSD1963_SIGNAL_RESET-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_CS<<SSD1963_SIGNAL_CS-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_DC<<SSD1963_SIGNAL_DC-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_RD<<SSD1963_SIGNAL_RD-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_WR<<SSD1963_SIGNAL_WR-1;
//
//	SSD1963_CONTROL_BUS->PUPDR |= SSD1963_SIGNAL_RESET<<SSD1963_SIGNAL_RESET-1;
//	SSD1963_CONTROL_BUS->PUPDR|= SSD1963_SIGNAL_CS<<SSD1963_SIGNAL_CS-1;
//	SSD1963_CONTROL_BUS->PUPDR |= SSD1963_SIGNAL_DC<<SSD1963_SIGNAL_DC-1;
//	SSD1963_CONTROL_BUS->PUPDR |= SSD1963_SIGNAL_RD<<SSD1963_SIGNAL_RD-1;
//	SSD1963_CONTROL_BUS->PUPDR |= SSD1963_SIGNAL_WR<<SSD1963_SIGNAL_WR-1;

	/*for 01 */ //output
	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_RESET<<SSD1963_SIGNAL_RESET-1;
	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_CS<<SSD1963_SIGNAL_CS-1;
	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_DC<<SSD1963_SIGNAL_DC-1;
	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_RD<<SSD1963_SIGNAL_RD-1;
	SSD1963_CONTROL_BUS->MODER |= SSD1963_SIGNAL_WR<<SSD1963_SIGNAL_WR-1;

	/*for 11 */ //50 MHz
	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_RESET<<SSD1963_SIGNAL_RESET-1 | SSD1963_SIGNAL_RESET<<SSD1963_SIGNAL_RESET;
	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_CS<<SSD1963_SIGNAL_CS-1 | SSD1963_SIGNAL_CS<<SSD1963_SIGNAL_CS;
	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_DC<<SSD1963_SIGNAL_DC-1 | SSD1963_SIGNAL_DC<<SSD1963_SIGNAL_DC;
	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_RD<<SSD1963_SIGNAL_RD-1 | SSD1963_SIGNAL_RD<<SSD1963_SIGNAL_RD;
	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_WR<<SSD1963_SIGNAL_WR-1 | SSD1963_SIGNAL_WR<<SSD1963_SIGNAL_WR;

	/*for 01 */ //10 MHz
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_RESET<<SSD1963_SIGNAL_RESET-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_CS<<SSD1963_SIGNAL_CS-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_DC<<SSD1963_SIGNAL_DC-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_RD<<SSD1963_SIGNAL_RD-1;
//	SSD1963_CONTROL_BUS->OSPEEDR |= SSD1963_SIGNAL_WR<<SSD1963_SIGNAL_WR-1;

	//SSD1963_CONTROL_BUS->MODER = 0x55555555UL; //All outputs
	//SSD1963_CONTROL_BUS->OSPEEDR = 0x55555555UL; //All medium speed (10 MHz)

}

void SSD_PowerOn(){
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_DISPLAY_ON);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

void SSD_SetPixel(uint16_t x, uint16_t y, uint32_t color){
	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(x >> 8); //First Col
	SSD_WriteData(x);
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) >> 8); //Last Col
	SSD_WriteData(TFT_HORIZONTAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(y >> 8); //First Page
	SSD_WriteData(y);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) >> 8); //Last Page
	SSD_WriteData(TFT_VERTICAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);


	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	///SSD_WriteData(color);
	//uint16_t data;
	//data = ((color & 0xF80000UL) >> 8) | ((color & 0x00FC00UL) >> 5) | ((color >> 3) & 0x00001FUL);
	SSD_WriteData(color);

	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

void SSD_SetArea(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2){

	/*IMPORTANT: 1 pizel [x=0,y=0], poslednij [x=479,y=271]
	 * toest x2 i y2 v etoi funkcii nuzno -1 brat, esli vivozu na ves ekran
	 */

	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(x1 >> 8); //First Col
	SSD_WriteData(x1);
	SSD_WriteData((x2) >> 8); //Last Col
	SSD_WriteData(x2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(y1 >> 8); //First Page
	SSD_WriteData(y1);
	SSD_WriteData((y2) >> 8); //Last Page
	SSD_WriteData(y2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	///SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	///GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	//	SSD_SetArea(0, TFT_HORIZONTAL_RESOLUTION - 1, 0, TFT_VERTICAL_RESOLUTION - 1);
	///SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	///GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	//v konce vivoda kartinki, nado cs vistavit v 1
	//\/GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

__attribute__( ( always_inline ) ) void SSD_WriteCommand(uint8_t command){
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);
	GPIO_Write(SSD1963_DATA_BUS, command);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	//delay_ms(1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	//GPIO_SetBits(SSD1963_DATA_BUS, SSD1963_SIGNAL_DC);
}

__attribute__( ( always_inline ) ) void SSD_WriteData(unsigned int data){
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);
	GPIO_Write(SSD1963_DATA_BUS, data);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	//delay_ms(1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	//GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);
	///GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

void SSD_fillScreen(uint32_t color){
	SSD_drawSquare(0, 0, TFT_HORIZONTAL_RESOLUTION - 1, TFT_VERTICAL_RESOLUTION - 1, color);
}

void SSD_drawSquare(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint32_t color){
	uint16_t x, y;

	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(x1 >> 8); //First Col
	SSD_WriteData(x1);
	SSD_WriteData(x2 >> 8); //Last Col
	SSD_WriteData(x2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(y1 >> 8); //First Page
	SSD_WriteData(y1);
	SSD_WriteData(y2 >> 8); //Last Page
	SSD_WriteData(y2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	for(y = y2 - y1 + 1; y>0; y--){
		for(x = x2 - x1 + 1; x>0; x--){
			GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);
			//SSD1963_DATA_BUS->ODR = ((color & 0xF80000UL) >> 8) | ((color & 0x00FC00UL) >> 5) | ((color >> 3) & 0x00001FUL);
			SSD1963_DATA_BUS->ODR = color;
			GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
			GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
		}
	}
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

}


void DrawImage(uint16_t *ptr,uint16_t *Circle_ptr)
{
	//TODO nado smesenija dobavit po x i y, opisanie
	int dx=0;

	SSD_SetArea(*(ptr+4)+*(ptr+0)+1,*(ptr+5)+*(ptr+2)+1,*(ptr+4)+*(ptr+1)-1,*(ptr+5)+*(ptr+3)-1);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	for(int i = 0; i<*(ptr+3) - *(ptr+2); i++)
		for(int j = 0; j<*(ptr+1) - *(ptr+0); j++){
			SSD_WriteData(*(Circle_ptr + dx));
			//SSD_WriteData(Circle[dx]);
			//SSD_SetPixel(i, j, Circle[dx]);
		dx++;
		//TIM7_delay_ms(100);
		}
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

}

LinearAproximationX_Y(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t *ValuesArrayX,uint16_t *ValuesArrayY)
{
	uint16_t i;
	uint16_t n, dx, dy, sgndx, sgndy, dxabs, dyabs, x, y, drawx, drawy;

	dx = x2 - x1;
	dy = y2 - y1;
	dxabs = (dx>0)?dx:-dx;
	dyabs = (dy>0)?dy:-dy;
	sgndx = (dx>0)?1:-1;
	sgndy = (dy>0)?1:-1;
	x = dyabs >> 1;
	y = dxabs >> 1;
	drawx = x1;
	drawy = y1;

	//gui->pset(drawx, drawy,c);
	*(ValuesArrayY+i)=drawy;
	*(ValuesArrayX+i)=drawx;
	i++;

	if( dxabs >= dyabs )
	{
		for( n=0; n<dxabs; n++ )
		{
			y += dyabs;
			if( y >= dxabs )
			{
				y -= dxabs;
				drawy += sgndy;
			}
			drawx += sgndx;
			//gui->pset(drawx, drawy,c);
			*(ValuesArrayY+i)=drawy;
			*(ValuesArrayX+i)=drawx;
			i++;
		}
	}
	else
	{
		for( n=0; n<dyabs; n++ )
		{
			x += dxabs;
			if( x >= dyabs )
		{
			x -= dyabs;
			drawx += sgndx;
		}
		drawy += sgndy;
		//gui->pset(drawx, drawy,c);
		*(ValuesArrayY+i)=drawy;
		*(ValuesArrayX+i)=drawx;
		i++;
		}
	}
}

LinearAproximationY(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t *ValuesArrayY)
{
	uint16_t i;
	uint16_t n, dx, dy, sgndx, sgndy, dxabs, dyabs, x, y, drawx, drawy;

	dx = x2 - x1;
	dy = y2 - y1;
	dxabs = (dx>0)?dx:-dx;
	dyabs = (dy>0)?dy:-dy;
	sgndx = (dx>0)?1:-1;
	sgndy = (dy>0)?1:-1;
	x = dyabs >> 1;
	y = dxabs >> 1;
	drawx = x1;
	drawy = y1;

	//gui->pset(drawx, drawy,c);
	*(ValuesArrayY+i)=drawy;
	i++;

	if( dxabs >= dyabs )
	{
		for( n=0; n<dxabs; n++ )
		{
			y += dyabs;
			if( y >= dxabs )
			{
				y -= dxabs;
				drawy += sgndy;
			}
			drawx += sgndx;
			//gui->pset(drawx, drawy,c);
			*(ValuesArrayY+i)=drawy;
			i++;
		}
	}
	else
	{
		for( n=0; n<dyabs; n++ )
		{
			x += dxabs;
			if( x >= dyabs )
		{
			x -= dyabs;
			drawx += sgndx;
		}
		drawy += sgndy;
		//gui->pset(drawx, drawy,c);
		*(ValuesArrayY+i)=drawy;
		i++;
		}
	}
}




