#define STM32_CLOCK_SPEED 72000000
#define Background_color C_MAROON
#define Menu_color C_CHART_REUSE
#define Button_color C_CHART_REUSE
#define SignalMesh_color C_GREEN_YELLOW
#define SignalBackground_color C_BLACK

#define MenuActive_color C_TEAL
#define MenuNonActive_color C_WHITE
#define MenuActiveText_color C_WHITE
#define MenuNonActiveText_color C_BLACK


#define IventRealButtonUp _INT_IVENT._EXTI0
#define IventRealButtonDown _INT_IVENT._EXTI4
#define IventRealButtonLeft _INT_IVENT._EXTI1
#define IventRealButtonRight _INT_IVENT._EXTI3
#define IventRealButtonCenter _INT_IVENT._EXTI2
#define IventTouchScreen _INT_IVENT._EXTI15_10

#include <stdio.h>
#include <stdlib.h>

#include "diag/Trace.h"

#include "stm32f30x_conf.h"

 char buf[200];
uint8_t TriggerFront=1; //1:rising 0:falling
uint8_t DigitAnalogGenerator=2; //1:digital analyzer 2:analog 3: sygnal generator

struct _INT_IVENT
{
	uint8_t _DMA1_Channel1;
	uint8_t _EXTI0;
	uint8_t _EXTI1;
	uint8_t _EXTI2;
	uint8_t _EXTI3;
	uint8_t _EXTI4;
	uint8_t _EXTI9_5;
	uint8_t _EXTI15_10;

}_INT_IVENT;


#include "ugui.c"
#include "IMAGES.c"
#include "TIMER.c"
#include "ADC.c"
#include "GPIO.c"
#include "DMA.c"
#include "EXTI.c"
#include "UART_USART.C"
#include "ssd1963.c"
#include "LCD_ELEMENTS.c"
#include "spi.c"
#include "XPT2046.c"
#include "menu.c"
#include "INTERRUPT.c"

void DrawPrimaryView( void );
void Draw_Signal_Area( void );












	// GPIOE->MODER = 0x55550000;  LED init port
	// 		 GPIOE->ODR = 0xF000;
	// trace_printf("_________________\n ");
