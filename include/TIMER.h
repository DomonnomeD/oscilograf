#ifndef __TIMER_H
#define __TIMER_H



/* For time counts ************************************************** */
#define TIM2_SPEED 2000 //Hz (1099 .. STM32_clock_speed)
#define TIM2_MAX_COUNT_VALUE 600 //max 0xFFFFFFFF
//Delay 0.5 sek = (STM32_CLOCK_SPEED/TIM2_SPEED+1)*TIM2_MAS_COUNT_VALUE/STM32_CLOCK_SPEED

void TIM2_init_CountCounter(void);
void TIM2_start(void);
int  TIM2_stop(void);




/* For PWM ********************************************************** */
#define TIM4_PWM_CHANNEL3_PD14_CH3  1
#define TIM4_PWM_CHANNEL3_PA13_CH3  0 //this pin working, but jtag return error becouse swdio for jtag this pin is
#define TIM4_PWM_CHANNEL3_PB8_CH3   0
// Period = TIM4_MAX_COUNT_VALUE*(STM32_clock_speed/TIM4_SPEED+1)/STM32_clock_speed = X sec
#define TIM4_SPEED 50000000 //hz
#define TIM4_MAX_COUNT_VALUE 600 //max 0xFFFFFFFF
#define TIM4_DUTY 300 //max TIM4_MAX_COUNT_VALUE

void PWM_TIM4_init(void);


/* TIM7 base dalay ************************************************** */
// TODO: nado kostil ispravit

int ValueForDelatLoop_ChangeInInterrupt=0;				//potom izpravit kostil

void TIM7_delay_ms(int _milisec); //nado kostil ispravit kogda nibud



/* External trigger for count Hz 1 chanel**************************** */
// TODO: nado dokoncit, vse rabotaet, no nado dopisat vrubanie prerivanie,
// TODO: i iz prerivanija vziatie znacenija

#define TIM3_EXT_TRIGGER_PD2   1
#define TIM3_EXT_TRIGGER_PB3   0

#define TIM3_EXT_TRIGGER_PRESCALER_0 1
#define TIM3_EXT_TRIGGER_PRESCALER_2 0
#define TIM3_EXT_TRIGGER_PRESCALER_4 0
#define TIM3_EXT_TRIGGER_PRESCALER_8 0 //if more than one init as 1 then init only first

#define TIM3_EXT_TRIGGER_FILTER 15 //filter from 0 .. 15

void TIM3_EXT_trigger(void);

/* External trigger for count Hz 2 chanel**************************** */
// TODO: nado sdelat dlia 2 kanalaese

void ADC_tim(int _milisec);

#endif
