#ifndef __ADC_H
#define __ADC_H


/* For first chanel of osciloscope ********************************** */
void ADC1_init(uint8_t _conversion_time);
void ADC1_TRIG_init(uint8_t _conversion_time);
void ADC12_speed(uint8_t _adc_speed);

/* For batery ******************************************************* */
void ADC2_init( void );
uint16_t ADC2_value( void );

/* For second chanel of osciloscope ********************************* */
void ADC3_init(uint8_t _time);
void ADC34_speed(uint8_t _adc_speed);

#endif
