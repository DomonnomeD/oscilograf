
int fot_test=0;		//for TIM2 interrupt, just fr test

/* For external trigger ********************************************* */
//void TIM3_IRQHandler( void)
//{
//	TIM3->SR &=~ TIM_SR_UIF;	//interrupt flug off
//}



/* For delay ******************************************************** */
// TODO: nado kostil ispravit
void TIM7_IRQHandler(void)
{
	TIM7->SR &=~ TIM_SR_UIF;	//interrupt flug off
	RCC->APB1ENR &= ~RCC_APB1ENR_TIM7EN;
	ValueForDelatLoop_ChangeInInterrupt=1;				//potom izpravit kostil
	///trace_printf("__________TIM7_IRQHandler tim7\n");
}


/* For ADC1 PA2 pin ************************************************* */
DMA1_Channel1_IRQHandler(void)
{
	//DMA1->IFCR |= DMA_ISR_GIF1;
	if (DMA_GetFlagStatus(DMA1_FLAG_GL1) != RESET){
		DMA_ClearITPendingBit(DMA1_FLAG_GL1); trace_printf("__________DMA1_FLAG_GL1 \n");}
	if (DMA_GetFlagStatus(DMA1_FLAG_TC1) != RESET){
		DMA_ClearITPendingBit(DMA1_FLAG_TC1); trace_printf("__________DMA1_FLAG_TC1 ");}
	if (DMA_GetFlagStatus(DMA1_FLAG_HT1) != RESET){
		DMA_ClearITPendingBit(DMA1_FLAG_HT1); trace_printf("__________DMA1_FLAG_HT1 ");}
	if (DMA_GetFlagStatus(DMA1_FLAG_TE1) != RESET){
		DMA_ClearITPendingBit(DMA1_FLAG_TE1); trace_printf("__________DMA1 transfer error \n");}

	trace_printf("\n__________DMA1_Channel1_IRQHandler adc1___\n");
	_INT_IVENT._DMA1_Channel1=1;
}

/* For ADC3 PE9 pin ************************************************* */
//todo proverit
//DMA2_Channel3_IRQHandler(void) //pohodu isibka
//								//kaztsia nado DMA2_Channel5_IRQHandler
//{
//	DMA_ClearITPendingBit(DMA_IT_TC);
//
//	DMA2_Channel1->CCR &= (~DMA_CCR_EN); //otrubanie dma, esli nevrubit v interrupt nepopadet bolse
//	trace_printf("__________DMA2_Channel1_IRQHandler interrut on\n");
//	//DMA2_Channel5->CCR |= DMA_CCR_EN;
//}

/* For UART4 TX pin ************************************************* */
DMA2_Channel5_IRQHandler(void)
{
	DMA_ClearITPendingBit(DMA_IT_TC);
	DMA_ClearITPendingBit(DMA_IT_HT);

	DMA2_Channel5->CCR &= (~DMA_CCR_EN);
	trace_printf("__________DMA2_Channel5_IRQHandler uart\n");
}


	/* all gpio interrupt */
#define Delay for(int i=0; i<800000; i++)

EXTI0_IRQHandler(void)
{
	trace_printf("__________interrupt gpio0\n");
	_SCREEN_TOUCH._BUTTON_UP_irq= 1;
	_INT_IVENT._EXTI0=1;
	Delay;
	EXTI->PR = EXTI_PR_PR0;
}

EXTI1_IRQHandler(void)
{
	trace_printf("__________interrupt gpio1\n");
	_SCREEN_TOUCH._BUTTON_LEFT_irq= 1;
	_INT_IVENT._EXTI1=1;
	Delay;
	EXTI->PR = EXTI_PR_PR1;
}

EXTI2_TS_IRQHandler(void)
{
	trace_printf("__________interrupt gpio2\n");
	_SCREEN_TOUCH._BUTTON_MENU_irq =1;
	_INT_IVENT._EXTI2=1;
	Delay;
	EXTI->PR = EXTI_PR_PR2;
}

EXTI3_IRQHandler(void)
{
	trace_printf("__________interrupt gpio3\n");
	_SCREEN_TOUCH._BUTTON_RIGHT_irq= 1;
	_INT_IVENT._EXTI3=1;
	Delay;
	EXTI->PR = EXTI_PR_PR3;
}

EXTI4_IRQHandler(void)
{
	trace_printf("__________interrupt gpio4\n");
	_SCREEN_TOUCH._BUTTON_DOWN_irq= 1;
	_INT_IVENT._EXTI4=1;
	Delay;
	EXTI->PR = EXTI_PR_PR4;
}

EXTI9_5_IRQHandler(void)
{
	trace_printf("__________interrupt gpio9_5\n");
	_INT_IVENT._EXTI9_5=1;
	EXTI->PR = EXTI_PR_PR9;
	EXTI->PR = EXTI_PR_PR8;
	EXTI->PR = EXTI_PR_PR7;
	EXTI->PR = EXTI_PR_PR6;
	EXTI->PR = EXTI_PR_PR5;
}



EXTI15_10_IRQHandler(void)
{
	uint16_t TipaDelay =100000;
	uint16_t TMPvalue=0;
	/*show tha we pressed button*/

	trace_printf("__________interrupt gpio15_10\n");
	_INT_IVENT._EXTI15_10 =1;

		/*get y*/
		/*send request for get y value*/
	GPIOC->ODR &= ~GPIO_ODR_13;
	uint16_t  timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0)){
		timeout--;
		}
	SPI_SendData8(SPI3, _start_bit | _y_pos);

	for(int i=0; i<TipaDelay ;i++); //tipa delay ... [funkcii nerabotajut v irq, pohodu izza jmp sliskom dlinnogo]
		/*spi receive byte*/
	timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0)){
		timeout--;
		}
	SPI_SendData8(SPI3, 0);

	timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_RXNE) == RESET) & (timeout != 0)){
		timeout--;
		}

	TMPvalue = SPI_ReceiveData8(SPI3);
	TMPvalue <<= 8; //value
		/*spi receive byte*/
	 timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0)){
		timeout--;
		}
	SPI_SendData8(SPI3, 0);

	timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_RXNE) == RESET) & (timeout != 0)){
		timeout--;
		}
	TMPvalue |= SPI_ReceiveData8(SPI3);
	TMPvalue >>= 3; //value

	TMPvalue -=250; //calibrate
	uint16_t TMP = TMPvalue;
	uint16_t Calibrat = 3800/272;
	uint16_t y_value = TMP/Calibrat;

	trace_printf("\n\n______y_value: %d_____\n",y_value);

		/*get x*/
	TMPvalue=0;
		/*sendbyte reauest for get x*/
	GPIOC->ODR &= ~GPIO_ODR_13;
	timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0)){
		timeout--;
		}
	SPI_SendData8(SPI3, _start_bit | _x_pos);

	for(int i=0; i<TipaDelay;i++); //tipa delay ...
		/*spi receive byte*/
	GPIOC->ODR &= ~GPIO_ODR_13;

	 timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0)){
		timeout--;
		}
	SPI_SendData8(SPI3, 0);

	timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_RXNE) == RESET) & (timeout != 0)){
		timeout--;
		}


	TMPvalue = SPI_ReceiveData8(SPI3);
	TMPvalue <<= 8; //value
		/*spi receive byte*/
	GPIOC->ODR &= ~GPIO_ODR_13;

	 timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0)){
		timeout--;
		}
	SPI_SendData8(SPI3, 0);

	timeout = TIMEOUT_TIME;
	while ((SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_RXNE) == RESET) & (timeout != 0)){
		timeout--;
		}
	TMPvalue |= SPI_ReceiveData8(SPI3);
	TMPvalue >>= 3; //value

	TMPvalue -=150; //calibrate
	 TMP = TMPvalue;
	 Calibrat = 4000/482;
	uint16_t x_value = TMP/Calibrat;

	trace_printf("\n\n______x_value: %d_____\n\n",x_value);


		if (x_value > _BUTTON_UP._X_Offset && x_value< _BUTTON_UP._X_Offset+_BUTTON_UP._X_R &&
				y_value> _BUTTON_UP._Y_Offset && y_value< _BUTTON_UP._Y_Offset+_BUTTON_UP._Y_R){
			trace_printf("\n\nBUTTON UP\n\n");
			_SCREEN_TOUCH._BUTTON_UP_irq+= 1;
		}

		if (x_value > _BUTTON_DOWN._X_Offset && x_value< _BUTTON_DOWN._X_Offset+_BUTTON_DOWN._X_R &&
				y_value> _BUTTON_DOWN._Y_Offset && y_value< _BUTTON_DOWN._Y_Offset+_BUTTON_DOWN._Y_R){
			trace_printf("\n\nBUTTON DOWN\n\n");
			_SCREEN_TOUCH._BUTTON_DOWN_irq+= 1;
		}

		if (x_value > _BUTTON_LEFT._X_Offset && x_value< _BUTTON_LEFT._X_Offset+_BUTTON_LEFT._X_R &&
				y_value> _BUTTON_LEFT._Y_Offset && y_value< _BUTTON_LEFT._Y_Offset+_BUTTON_LEFT._Y_R){
			trace_printf("\n\nBUTTON LEFT\n\n");
			_SCREEN_TOUCH._BUTTON_LEFT_irq+= 1;
		}

		if (x_value > _BUTTON_RIGHT._X_Offset && x_value< _BUTTON_RIGHT._X_Offset+_BUTTON_RIGHT._X_R &&
				y_value> _BUTTON_RIGHT._Y_Offset && y_value< _BUTTON_RIGHT._Y_Offset+_BUTTON_RIGHT._Y_R){
			trace_printf("\n\nBUTTON RIGHT\n\n");
			_SCREEN_TOUCH._BUTTON_RIGHT_irq+= 1;
		}

		if (x_value > _SIGNAL_AREA._Left_X_L && y_value >_SIGNAL_AREA._Left_Y_L &&
				x_value <_SIGNAL_AREA._Left_X_R && y_value<_SIGNAL_AREA._Left_Y_R){
			trace_printf("\n\nsignal area left\n\n");
			_SCREEN_TOUCH._SIGNAL_AREA_LEFT_irq +=1;
		}

		if (x_value > _SIGNAL_AREA._Center_Up_X_L && y_value >_SIGNAL_AREA._Center_Up_Y_L &&
				x_value <_SIGNAL_AREA._Center_Up_X_R && y_value<_SIGNAL_AREA._Center_Up_Y_R){
			trace_printf("\n\nsignal area center up\n\n");
			_SCREEN_TOUCH._SIGNAL_AREA_CENTER_UP_irq +=1;
		}

		if (x_value > _SIGNAL_AREA._Center_Down_X_L && y_value >_SIGNAL_AREA._Center_Down_Y_L &&
				x_value <_SIGNAL_AREA._Center_Down_X_R && y_value<_SIGNAL_AREA._Center_Down_Y_R){
			trace_printf("\n\nsignal area center down\n\n");
			_SCREEN_TOUCH._SIGNAL_AREA_CENTER_DOWN_irq +=1;
		}

		if (x_value > _SIGNAL_AREA._Right_X_L && y_value >_SIGNAL_AREA._Right_Y_L &&
				x_value <_SIGNAL_AREA._Right_X_R && y_value<_SIGNAL_AREA._Right_Y_R){
			trace_printf("\n\nsignal area right\n\n");
			_SCREEN_TOUCH._SIGNAL_AREA_RIGHT_irq +=1;
		}

		if(x_value > (_MENU_CIRCLE._X-_MENU_CIRCLE._R) &&y_value > _MENU_CIRCLE._Y &&
				x_value < _MENU_CIRCLE._X && y_value < _MENU_CIRCLE._R){
			trace_printf("\n\nmenu area\n\n");
			_SCREEN_TOUCH._BUTTON_MENU_irq +=1;
		}





		/*show that we CAN UNpressed button*/
//	 UG_SetForecolor ( C_WHITE ) ;
//	 UG_PutString(_INFO_COUT_AREA._X,_INFO_COUT_AREA._Y,"                                                   ");
//	 UG_PutString(_INFO_COUT_AREA._X,_INFO_COUT_AREA._Y,buf);
//	 	UG_SetForecolor ( C_WHITE ) ;
//		 UG_PutString(_INFO_COUT_AREA._X,_INFO_COUT_AREA._Y,"                                                   ");
//		 UG_PutString(_INFO_COUT_AREA._X,_INFO_COUT_AREA._Y,buf);


for(int i=0; i<2000; i++);

	EXTI->PR = EXTI_PR_PR15;
	EXTI->PR = EXTI_PR_PR14;
	EXTI->PR = EXTI_PR_PR13;
	EXTI->PR = EXTI_PR_PR12;
	EXTI->PR = EXTI_PR_PR11;
	EXTI->PR = EXTI_PR_PR10;
}




