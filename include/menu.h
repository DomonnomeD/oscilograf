typedef enum {LEFT= 0, RIGHT = 1, NONE = 2} MenuManagerHandler;


struct _SCREEN_TOUCH
{
	uint8_t _BUTTON_LEFT_irq;
	uint8_t _BUTTON_RIGHT_irq;
	uint8_t _BUTTON_UP_irq;
	uint8_t _BUTTON_DOWN_irq;

	uint8_t _SIGNAL_AREA_CENTER_UP_irq;
	uint8_t _SIGNAL_AREA_CENTER_DOWN_irq;
	uint8_t _SIGNAL_AREA_LEFT_irq;
	uint8_t _SIGNAL_AREA_RIGHT_irq;

	uint8_t _BUTTON_MENU_irq;

}_SCREEN_TOUCH;

struct _MENU_MANAGER_HANDLER
{
	double VoltageTrigger;
	uint8_t TruggerUpDown;
	uint8_t DrawYesNo;
	double FreqTrigger;


}_MENU_MANAGER_HANDLER;
