#include "UART_USART.h"
/*
 * UART4_init - init uart4
 *
 * @INTPUT: none
 * @OUTPUT: none
 *
 * send egzample:
 * 		while (USART_GetFlagStatus(UART4, USART_FLAG_TC) == RESET);
 *		USART_SendData(UART4, tmp);
 *
 * receive egzample:
 * 		while (USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET);
 * 		uint16_t tmp = USART_ReceiveData(UART4);
 *
 */
void UART4_init()
{
	 GPIO_InitTypeDef GPIO_usart4;

	 GPIO_usart4.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
	 GPIO_usart4.GPIO_Mode = GPIO_Mode_AF;
	 GPIO_usart4.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_usart4.GPIO_OType = GPIO_OType_PP;
	 GPIO_usart4.GPIO_PuPd = GPIO_PuPd_NOPULL;

	 GPIO_Init(GPIOC, &GPIO_usart4);

	 GPIO_PinAFConfig(GPIOC,GPIO_PinSource10,GPIO_AF_5);
	 GPIO_PinAFConfig(GPIOC,GPIO_PinSource11,GPIO_AF_5);

	 RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4,ENABLE);

	 USART_InitTypeDef USART_InitStruct;

	 USART_InitStruct.USART_BaudRate = 9600;
	 USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	 USART_InitStruct.USART_StopBits = USART_StopBits_1;
	 USART_InitStruct.USART_Parity = USART_Parity_No ;
	 USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	 USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

	 USART_Init(UART4, &USART_InitStruct);

	 USART_Cmd(UART4, ENABLE);

}
