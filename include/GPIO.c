#include "GPIO.h"

/* ****************************************************************** */
/*
 * 				For now, init all pins
 */
/* ****************************************************************** */
// TODO: nado sdelat dlia kazdoi periferii svoju inicializciju

void GPIO_init(void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOF,ENABLE);
//	RCC->AHBENR |= RCC_AHBENR_GPIOEEN;		//clock E
//	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;		//clock A
}
