#include "XPT2046.h"

xpt2046_init( void )
{

 	spi_sendByte(SPI3,_start_bit); //init
 	spi_receiveByte(SPI3); //ignore
 	spi_receiveByte(SPI3); //ignore
    //trace_printf("1 byte init %d\n ",spi_receiveByte(SPI3));
   // trace_printf("2 byte init %d\n ",spi_receiveByte(SPI3));

}

uint16_t xpt2046_get_x( void )
{
	uint16_t TMPvalue=0;

	spi_sendByte(SPI3,_start_bit | _x_pos); //zapros na x
	TIM7_delay_ms(1); //15 cycle wait for ADC conversion
	TMPvalue = spi_receiveByte(SPI3);
	TMPvalue <<= 8;
	TMPvalue |= spi_receiveByte(SPI3);
	TMPvalue >>= 3;

	TMPvalue -=150; //calibrate

	return TMPvalue;
}

uint16_t xpt2046_get_y( void )
{
	uint16_t TMPvalue=0;

	spi_sendByte(SPI3,_start_bit | _y_pos); //zapros na y
	TIM7_delay_ms(1); //15 cycle wait for ADC conversion
	TMPvalue = spi_receiveByte(SPI3);
	TMPvalue <<= 8;
	TMPvalue |= spi_receiveByte(SPI3);
	TMPvalue >>= 3;

	TMPvalue -=250; //calibrate

	return TMPvalue;
}

uint16_t xpt2046_get_LCD_y( void )
{
	uint16_t TMP = xpt2046_get_y();

	uint16_t Calibrat = 3800/272;

	return TMP/Calibrat;

}
uint16_t xpt2046_get_LCD_x( void )
{
	uint16_t TMP = xpt2046_get_x();

	uint16_t Calibrat = 4000/482;

	return TMP/Calibrat;

}
