#include "ADC.h"



/*
 * ADC1_init - init ADC1, chanel 3, PA2 pin
 *
 * @_conversion_time: converion time 0..7
 * @OUTPUT: none
 *
 * The larger the value @_conversion_time, the less ADC speed
 *
 * @IMPORTANT! for some reason don't correct working gpio setting,
 * 		but it's works, for now ... , so, if get strange error, need check gpio
 * @IMPORTANT! need be carefully with rank!!!!
 */
void ADC1_init(uint8_t _conversion_time)
{

	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, DISABLE);
	 RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR2; //50mhZ

 	 GPIOA->PUPDR |= GPIO_PUPDR_PUPDR2_1; //pull down
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR3; //50MHz
	 GPIOA->MODER |= GPIO_MODER_MODER2; //analog
//GPIO_InitTypeDef  GPIO_InitStructure;
//	    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
//	    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//	    GPIO_InitStructure.GPIO_PuPd = GPIO_PUPDR_PUPDR2_1;
//	    GPIO_Init(GPIOA, &GPIO_InitStructure); //pocemuto pupd viletaet v assert


	 	 //* ADC *//
 	RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2);
	ADC_StructInit(&ADC_InitStructure);
 	ADC_VoltageRegulatorCmd(ADC1, ENABLE); //calibration
	ADC_SelectCalibrationMode(ADC1, ADC_CalibrationMode_Single);
	 ADC_StartCalibration(ADC1);
	 	 //ADC init loop
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_OneShot;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;
	 ADC_CommonInit(ADC1, &ADC_CommonInitStructure);

	//* DMA ADC1 *//
	 ADC_DMACmd(ADC1, ENABLE);
	 ADC_DMAConfig(ADC1, ADC_DMAMode_Circular);

	 while(ADC_GetCalibrationStatus(ADC1) != RESET );

	 	 //* ADC *//
	 ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
	 ADC_InitStructure.ADC_Resolution = ADC_Resolution_8b;
	 ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
	 //ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_RisingEdge;
	 ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
	 ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	 ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
	 ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
	 ADC_InitStructure.ADC_NbrOfRegChannel = 1;
	 ADC_Init(ADC1, &ADC_InitStructure);

	 ADC_RegularChannelConfig(ADC1, 3, 1, (uint8_t)_conversion_time); //3 chanel, 1 rank
	 	 	 	 	 	 	 	 //conversion time - time for charge embedded capacitor to source voltage level

	 ADC_Cmd(ADC1, ENABLE); //ADC1 on

	 while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY));

	 ADC_StartConversion(ADC1);

}

/*
 * ADC1_TRIG_init - init ADC1, chanel 3, PA2 pin
 *working only by trigger TIM15
 *
 * @_conversion_time: converion time 1..8
 * @OUTPUT: none
 *
 * The larger the value @_conversion_time, the less ADC speed
 *
 * @IMPORTANT! for some reason don't correct working gpio setting,
 * 		but it's works, for now ... , so, if get strange error, need check gpio
 * @IMPORTANT! need be carefully with rank!!!!
 */
void ADC1_TRIG_init(uint8_t _conversion_time)
{

	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

	 RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR2; //50mhZ
 	 GPIOA->PUPDR |= GPIO_PUPDR_PUPDR2_1; //pull down
	 GPIOA->MODER |= GPIO_MODER_MODER2; //analog

	 	 //* ADC *//
 	RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2);
	ADC_StructInit(&ADC_InitStructure);
 	ADC_VoltageRegulatorCmd(ADC1, ENABLE); //calibration
	ADC_SelectCalibrationMode(ADC1, ADC_CalibrationMode_Single);
	 ADC_StartCalibration(ADC1);
	 	 //ADC init loop
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_OneShot;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;
	 ADC_CommonInit(ADC1, &ADC_CommonInitStructure);

	//* DMA ADC1 *//
	 ADC_DMACmd(ADC1, ENABLE);
	 ADC_DMAConfig(ADC1, ADC_DMAMode_Circular);

	 while(ADC_GetCalibrationStatus(ADC1) != RESET );

	 	 //* ADC *//
	 //ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
	 ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Disable;
	 ADC_InitStructure.ADC_Resolution = ADC_Resolution_8b;
	 ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_14;
	 ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_RisingEdge;
	 //ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
	 ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	 ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
	 ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
	 ADC_InitStructure.ADC_NbrOfRegChannel = 1;
	 ADC_Init(ADC1, &ADC_InitStructure);

	 ADC_RegularChannelConfig(ADC1, 3, 1, IS_ADC_SAMPLE_TIME(_conversion_time)); //3 chanel, 1 rank

	 ADC_Cmd(ADC1, ENABLE); //ADC1 on

	 while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY));

	 ADC_StartConversion(ADC1);

}


/*
 * ADC12_speed - change ADC12 sampling time
 *
 * @_adc_speed: value from 0.11
 * @OUTPUT: none
 *
 * The larger the value @_adc_speed, the less ADC conversion speed
 * 		but better accuracy
 */
void ADC12_speed(uint8_t _adc_speed)
{
	RCC_ADCCLKConfig( (_adc_speed<<4) | 0b100000000  );
}


/*
 * ADC2_init - init ADC2, chanel 5, PC4 pin
 *
 * @INPUT: none
 * @OUTPUT: none
 *
 * @IMPORTANT! converion TIME ADC1 and ADC2 the same
 */
void ADC2_init( void )
{

	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

//GPIO_InitTypeDef  GPIO_InitStructure;
//	    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
//	    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//	    GPIO_InitStructure.GPIO_PuPd = GPIO_PUPDR_PUPDR4_1;
//	    GPIO_Init(GPIOC, &GPIO_InitStructure); //pocemuto pupd viletaet v assert


    //* ADC *//
    RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2); /*OPASNOE MESTO, ona odnovremeno meniaet i adc1 kanala paremtri /
    										//no odnovremeno i nemozet bit virublenoi(zakomencenoi), tak kak /
    										//nepodajutsia takti na nee esli toko adc2 vistavlen rabotat*/
    ADC_StructInit(&ADC_InitStructure);
    ADC_VoltageRegulatorCmd(ADC2, ENABLE); //calibration
    ADC_SelectCalibrationMode(ADC2, ADC_CalibrationMode_Single);
    ADC_StartCalibration(ADC2);
    //ADC loop
    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_OneShot;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;
    ADC_CommonInit(ADC2, &ADC_CommonInitStructure);

    //* DMA ADC1 *//
    //ADC_DMACmd(ADC2, ENABLE);
   // ADC_DMAConfig(ADC2, ADC_DMAMode_Circular);

    while(ADC_GetCalibrationStatus(ADC2) != RESET );

    //* ADC *//
    ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Disable;
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_8b;
    ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
    ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
    ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
    ADC_InitStructure.ADC_NbrOfRegChannel = 1;
    ADC_Init(ADC2, &ADC_InitStructure);

    ADC_RegularChannelConfig(ADC2, 5, 1, IS_ADC_SAMPLE_TIME(1)); //5 chanel, 1 rank

    ADC_Cmd(ADC2, ENABLE); //ADC1 on

    while(!ADC_GetFlagStatus(ADC2, ADC_FLAG_RDY));

    ADC_StartConversion(ADC2);

}

/*
 * ADC2_value - return ADC2 value
 *
 * @INPUT: none
 * @OUTPUT: value
 */
uint16_t ADC2_value(void)
{
	ADC_StartConversion(ADC2);
	return ADC2->DR;
}


/*
 * ADC3_init - init ADC3, chanel 7, PD10 pin
 *
 * @_time: converion time 1..8
 * @OUTPUT: none
 *
 * The larger the value @_conversion_time, the less ADC speed
 *
 * @IMPORTANT! for some reason don't correct working gpio setting,
 * 		but it's works, for now ... , so, if get strange error, need check gpio
 * @IMPORTANT! need be carefully with rank!!!!
 *
 * read egzample:
 * 		ADC_StartConversion(ADC3);
 * 		trace_printf(" %d \n ",ADC3->DR);
 */
void ADC3_init(uint8_t _time)
{

	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC34, ENABLE);

//GPIO_InitTypeDef  GPIO_InitStructure;
//	    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
//	    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//	    GPIO_InitStructure.GPIO_PuPd = GPIO_PUPDR_PUPDR9_1;
//	    GPIO_Init(GPIOD, &GPIO_InitStructure); //pocemuto pupd viletaet v assert


    //* ADC *//
    RCC_ADCCLKConfig(RCC_ADC34PLLCLK_Div2);
    ADC_StructInit(&ADC_InitStructure);
    ADC_VoltageRegulatorCmd(ADC3, ENABLE);
    ADC_SelectCalibrationMode(ADC3, ADC_CalibrationMode_Single);
    ADC_StartCalibration(ADC3);
    //ADC loop
    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_OneShot;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;
    ADC_CommonInit(ADC3, &ADC_CommonInitStructure);

    //* DMA ADC3 *//
    ADC_DMACmd(ADC3, ENABLE);
    ADC_DMAConfig(ADC3, ADC_DMAMode_Circular);

    while(ADC_GetCalibrationStatus(ADC3) != RESET );


    //* ADC *//
    ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
    ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
    ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
    ADC_InitStructure.ADC_NbrOfRegChannel = 1;
    ADC_Init(ADC3, &ADC_InitStructure);

    ADC_RegularChannelConfig(ADC3, 7, 1, IS_ADC_SAMPLE_TIME(_time)); //2 chanel, 1 rank
    				/* opasnoe mesto, pocemuto esli adc toko odin vkliucen, to esli rank = 2, nerobit */
    ADC_Cmd(ADC3, ENABLE);

    while(!ADC_GetFlagStatus(ADC3, ADC_FLAG_RDY));
    ADC_StartConversion(ADC3);

}

/*
 * ADC34_speed - change ADC34 sampling time
 *
 * @_adc_speed: value from 1.11
 * @OUTPUT: none
 *
 * The larger the value @_adc_speed, the less ADC conversion speed
 * 		but better accuracy
 */
void ADC34_speed(uint8_t _adc_speed)
{

	RCC_ADCCLKConfig( (1<<7*4)|(1<<13)|(_adc_speed<<9) );

}




