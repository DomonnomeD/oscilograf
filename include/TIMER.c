#include "TIMER.h"


/*
 * TIM2_init_CountCounter - TIM2 base ticks counter init
 *
 * @INPUT: none
 * @OUTPUT: none
 *
 * TIM2 have 32 bit counter register!!!
 * This function works with TIM2_start() and TIM2_stop()
 */
void TIM2_init_CountCounter(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;		//Timer 2 clock on
//	NVIC_EnableIRQ(TIM2_IRQn );	//global for TIM2 interrupt enable
	TIM2->CR1 &=~TIM_CR1_DIR;	//Counter used as upcounter (1,2,3...)
	TIM2->CR1 &=~TIM_CR1_ARPE;	//value for TIMx_ARR register write imediately
	TIM2->PSC = STM32_CLOCK_SPEED/TIM2_SPEED;	//prescaler for timer counts max 0xFFFF
//	TIM2->CR1 |=TIM_CR1_CEN;	//counter enabled
	// work with auto-reload module
	TIM2->ARR = TIM2_MAX_COUNT_VALUE;
//	TIM2->DIER |=TIM_DIER_UIE;	//Update interrupt enabled (overflow reload register)
	// work with capture compared module
//	TIM2->CCR2 |=0xFFFF;				//value for capture/compare register
//	TIM2->DIER |= TIM_DIER_CC2IE;		//capture-compared interrupt enable
}

/*
 * TIM2_start - lounche counter
 *
 * @INPUT: none
 * @OUTPUT: none
 *
 * This function works with TIM2_init_CountCounter() and TIM2_stop()
 */
void TIM2_start(void)
{
	TIM2->CNT = 0;	//reset counter value
	TIM2->CR1 |=TIM_CR1_CEN;	//counter enabled
}

/*
 * TIM2_stop - stop count's end return time in ticks
 *
 * @INPUT: none
 * @OUTPUT: ticks
 *
 * This function works with TIM2_init_CountCounter() and TIM2_start()
 */
// TODO: nado sdelat stob sekundami vozvrasala
int TIM2_stop(void)
{
//	if ((TIM2->SR & TIM_SR_UIF)==1)
//	{
//		TIM2->SR &=~ TIM_SR_UIF;	//interrupt flug off
		TIM2->CR1 &= ~TIM_CR1_CEN;
		return TIM2->CNT; // 1/(TIM2->CNT) /*Popravit*/
//	}
//	else
//	{
//		return 0;	//overload
//	}
}


/*
 * PWM_TIM4_init - TIM4 base PWM init, PD14,PA13,PB8
 *
 * @INPUT: none
 * @OUTPUT: none
 */

void PWM_TIM4_init()
{

#if TIM4_PWM_CHANNEL3_PD14_CH3

	GPIOD->MODER |=GPIO_MODER_MODER14_1; //set alternative function for pin PD14

	GPIOD->AFR[1] |=  33554432;	//Select AF2 for PD14: TIM4_CH3

#endif

#if TIM4_PWM_CHANNEL3_PA13_CH3

	GPIOA->MODER |=GPIO_MODER_MODER13_1;

	GPIOA->AFR[1] |=  10485760 ;	//Select AF10 for PA13: TIM4_CH3

#endif

#if TIM4_PWM_CHANNEL3_PB8_CH3

	GPIOB->MODER |=GPIO_MODER_MODER8_1;

	GPIOB->AFR[1] |=  2 ;	//Select AF2 for PB8: TIM4_CH3

#endif

	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; // Enable Timer 2 clock
	TIM4->PSC = STM32_CLOCK_SPEED/TIM4_SPEED; // Set prescaler to 6000 (PSC + 1)
	TIM4->ARR = TIM4_MAX_COUNT_VALUE; // Auto reload value 600
	TIM4->CCR3 = TIM4_DUTY; // Start PWM duty for channel 3
	TIM4->CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1; // PWM mode 1 on channel 3
	TIM4->CCER |= TIM_CCER_CC3E; // Enable compare on channel 3
	TIM4->CR1 |= TIM_CR1_CEN; // Enable timer

}


/*
 * TIM7_delay - TIM7 base dalay
 *
 * @_milisec: delay time in milisec
 * @OUTPUT; none
 */
// TODO: nado kostil ispravit
//todo sdelat, stob rabotalo na raznih castotah
void TIM7_delay_ms(int _milisec) //int sek
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM7EN; // Enable Timer 7 clock

	TIM7->CR1 &= ~TIM_CR1_CEN; //clock timer
	TIM7->CNT = 0;

	ValueForDelatLoop_ChangeInInterrupt=0;				//potom izpravit kostil

	NVIC_EnableIRQ(TIM7_IRQn );

	//TIM7->CNT |= 0xFFFFFF; ///max 0xFFFFFFFF
	TIM7->PSC = 4799; //max 0xFFFF
	TIM7->ARR = _milisec*15; //0xFFFFFFFF
	TIM7->CR1 |= TIM_CR1_CEN; //clock timer
	TIM7->DIER |= TIM_DIER_UIE; //Update interrupt enabled.

	while(ValueForDelatLoop_ChangeInInterrupt == 0) {};
//	while((TIM7->SR && TIM_SR_UIF) == 0) {};			//potom izpravit kostil
//	TIM7->SR &=~ TIM_SR_UIF;
}





/*
 * TIM3_EXT_trigger - init TIM3 base trigger
 *
 * @INPUT: none
 * @OUTPUT: none
 */
// TODO: nado dokoncit, vse rabotaet, no nado dopisat vrubanie prerivanie,
// TODO: i iz prerivanija vziatie znacenija

void TIM3_EXT_trigger(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;

#if TIM3_EXT_TRIGGER_PD2

	GPIOD->MODER |=GPIO_MODER_MODER2_1;
	GPIOD->AFR[0] |=  512 ;

#endif

#if TIM3_EXT_TRIGGER_PB3

	GPIOB->MODER |=GPIO_MODER_MODER3_1;
	GPIOB->AFR[0] |=  40960 ;

#endif

	//461 reference
#if TIM3_EXT_TRIGGER_PRESCALER_0
	TIM3->SMCR &= ~TIM_SMCR_ETPS;
#elif TIM3_EXT_TRIGGER_PRESCALER_2
	TIM3->SMCR |= TIM_SMCR_ETPS_0;
#elif TIM3_EXT_TRIGGER_PRESCALER_4
	TIM3->SMCR |= TIM_SMCR_ETPS_1;
#elif TIM3_EXT_TRIGGER_PRESCALER_8
	TIM3->SMCR |= TIM_SMCR_ETPS;
#endif

	TIM3->SMCR &= ~TIM_SMCR_ETF; //filter init

	TIM3->SMCR |= (TIM_SMCR_ETF & (TIM3_EXT_TRIGGER_FILTER << 8));

	TIM3->SMCR &= ~TIM_SMCR_ETP; //detection rising edge
	TIM3->SMCR |= TIM_SMCR_ECE; //external clock enable

	//timer init as usualy
	TIM3->PSC = 0; //max 0xFFFF
	TIM3->ARR = 15000; //0xFFFFFFFF
	//timer on
	TIM3->CR1 |= TIM_CR1_CEN;
}


/*
 * ADC_tim - init TIM15 for as ADC triger
 *
 * @INPUT: _milisec
 * @OUTPUT: none
 */
void ADC_tim(int _milisec)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM15EN;

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 600;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 36000;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter =100;
	TIM_TimeBaseInit(TIM15,&TIM_TimeBaseInitStruct);

//	TIM15->ARR = TIM_TimeBaseInitStruct->TIM_Period ;
//	TIM15->PSC = TIM_TimeBaseInitStruct->TIM_Prescaler;
//	TIM15->RCR = TIM_TimeBaseInitStruct->TIM_RepetitionCounter;
	TIM15->ARR = _milisec*15;
	TIM15->PSC = 4799;
	TIM15->RCR = 52000;


	//TIM15->ARR=600;
	//TIM_SetAutoreload(TIM15,ENABLE);

//	TIM15->CNT; //value timer

	TIM_SelectOutputTrigger(TIM15,TIM_TRGOSource_Update);
	TIM_Cmd(TIM15, ENABLE);
}


