#ifndef __DMA_H
#define __DMA_H

/* For first chanel of osciloscope ********************************** */
void DMA1_ADC1_init(uint16_t *_adc_destination,uint16_t _buff_size);

/* For second chanel of osciloscope ********************************* */
void DMA2_ADC3_init(uint16_t *_adc_destination,uint16_t _buff_size);

#endif
