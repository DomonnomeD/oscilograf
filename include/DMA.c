#include "DMA.h"


/*
 * DMA1_ADC1_init - init DMA1 for ADC1
 *
 * @_adc_destination: mas or variable, in which wil be stored ADC1 value
 * @_buff_size: size of mas or variable
 * @OUTPUT: none
 *
 * if @_adc_destination is varialbe, then @_buff_size = 1,
 * 		if _adc_destination[n], then @_buff_size = n
 */
void DMA1_ADC1_init(uint16_t *_adc_destination,uint16_t _buff_size){

	DMA_Cmd(DMA1_Channel1, DISABLE); //important if want change values after on
	//DMA_SetCurrDataCounter(DMA1_Channel1, SiZe);
	//DMA_Cmd(DMA1_Channel1, ENABLE);


	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	DMA_InitTypeDef DMA_InitStructure;


    //* DMA *//
    DMA_DeInit(DMA1_Channel1);

    DMA_InitStructure.DMA_PeripheralBaseAddr = &(ADC1->DR); //take data from ADC1
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&_adc_destination[0]; //ADC1 -> mas
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; //perifery to memory
    DMA_InitStructure.DMA_BufferSize = _buff_size; //buffer size
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //perifery adress NOT increment
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; //memory adress increment
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; //8bit perifery
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord; //8 bit memory
    //DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);

    /* Enable DMA1 Channel1 transfer */
    DMA_Cmd(DMA1_Channel1, ENABLE);

    /* Enable DMA1 channel1 IRQ Channel */
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Enable DMA1 Channel1 Transfer Complete interrupt */
    DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);
    //DMA_ITConfig(DMA1_Channel1, DMA_IT_HT, ENABLE);
    DMA_ITConfig(DMA1_Channel1, DMA_IT_TE, ENABLE);


   // NVIC_EnableIRQ(DMA1_Channel1_IRQn );

}

/*
 * DMA2_ADC3_init - init DMA2 for ADC3
 *
 * @_adc_destination: mas or variable, in which wil be stored ADC1 value
 * @_buff_size: size of mas or variable
 * @OUTPUT: none
 *
 * if @_adc_destination is varialbe, then @_buff_size = 1,
 * 		if _adc_destination[n], then @_buff_size = n
 */
//todo pereproverit, tocno li etot kanal chanel3
//todo PEREPROVETIR VSIU FUNCIJU
void DMA2_ADC3_init(uint16_t *_adc_destination,uint16_t _buff_size){

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
	DMA_InitTypeDef DMA_InitStructure;


    //* DMA *//
    DMA_DeInit(DMA2_Channel5);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(ADC3->DR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&_adc_destination[0];
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = _buff_size;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;

    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA2_Channel5, &DMA_InitStructure);
    //DMA_ITConfig(DMA1_Channel1, DMA_IT_HT, ENABLE); //interrupt then buffer overflow
    //NVIC_EnableIRQ(DMA1_Channel1_IRQn); //enable interrupt
    DMA_Cmd(DMA2_Channel5, ENABLE); //DMA1 on

	NVIC_EnableIRQ(DMA2_Channel1_IRQn );
	DMA_ITConfig(DMA2_Channel5, DMA_IT_TC, ENABLE);

}



















////////////////////////////////////
//todo poka nenastoreno dokonca

void DMA2_UART4_TX_init(uint16_t *bufer,uint16_t _buff_size)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
	DMA_InitTypeDef DMA_InitStructure;


    //* DMA *//
    DMA_DeInit(DMA2_Channel5);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(UART4->TDR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&bufer[0];
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST; //from bufer to TX port
    DMA_InitStructure.DMA_BufferSize = _buff_size;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_Low;

    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA2_Channel5, &DMA_InitStructure);

    DMA_ITConfig(DMA2_Channel5, DMA_IT_HT, ENABLE); //interrupt then buffer overflow
    NVIC_EnableIRQ(DMA2_Channel5_IRQn); //enable interrupt

    DMA_Cmd(DMA2_Channel5, ENABLE); //DMA2 on


    USART_DMACmd(UART4,USART_DMAReq_Tx,ENABLE);


	//NVIC_EnableIRQ(DMA2_Channel5_IRQn );
	//DMA_ITConfig(DMA2_Channel5, DMA_IT_TC, ENABLE);

}

