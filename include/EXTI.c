#include "EXTI.h"
/*
 * EXTI_Config - configure pin's for external interrupt
 *
 * @EXTI_PortSourceGPIOx: GPIO pins
 * 							EXTI_PortSourceGPIOA
 * 							EXTI_PortSourceGPIOB
 * 							EXTI_PortSourceGPIOC
 * 							EXTI_PortSourceGPIOD
 * 							EXTI_PortSourceGPIOE
 * 							EXTI_PortSourceGPIOF
 * @EXTI_PinSourcex: Pin number
 * 							EXTI_PinSource0
 * 							EXTI_PinSource2
 * 							EXTI_PinSource3
 * 							EXTI_PinSource4
 * 							EXTI_PinSource5
 * 							EXTI_PinSource6
 * 							EXTI_PinSource7
 * 							EXTI_PinSource8
 * 							EXTI_PinSource9
 * 							EXTI_PinSource10
 * 							EXTI_PinSource11
 * 							EXTI_PinSource12
 * 							EXTI_PinSource13
 * 							EXTI_PinSource14
 * 							EXTI_PinSource15
 * @OUTPUT: none
 *
 * @IMPORTANT! for some reason, PinSource2 dont correct working
 */
void EXTI_Config(uint8_t EXTI_PortSourceGPIOx, uint8_t EXTI_PinSourcex,FunctionalStateMy NewState)
{
	RCC->APB2ENR |= RCC_APB2Periph_SYSCFG;

	//GPIOE->PUPDR |=2<<EXTI_PinSourcex*2; //pull-down

	EXTI->IMR |= 1<<EXTI_PinSourcex; //interrupt mask
	if (NewState == RISING)
		EXTI->RTSR |= (1<<EXTI_PinSourcex); //rising trigger
	else if (NewState == FALLING)
		EXTI->FTSR |= (1<<EXTI_PinSourcex); //failing trigger
	else if(BOTH == BOTH){
		EXTI->RTSR |= (1<<EXTI_PinSourcex); //rising trigger
		EXTI->FTSR |= (1<<EXTI_PinSourcex); //failing trigger
	}

	SYSCFG->EXTICR[EXTI_PinSourcex >> 0x02] |= (((uint32_t)EXTI_PortSourceGPIOx) << (0x04 * (EXTI_PinSourcex & (uint8_t)0x03)));

	if (EXTI_PinSourcex < 5)
		NVIC_EnableIRQ (EXTI_PinSourcex+6);
	else if(EXTI_PinSourcex<10)
		NVIC_EnableIRQ (23);
	else
		NVIC_EnableIRQ (40);
}
