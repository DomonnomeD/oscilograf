#include "spi.h"

void spi3_gpio_init()
{
	GPIO_InitTypeDef gpio;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);

    // SPI:  PA5, PA6, PA7
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_6);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_6);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_6);

    gpio.GPIO_Mode = GPIO_Mode_AF;
    gpio.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOC, &gpio);

    //ns(cs) pin vrucnuju
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_Pin = GPIO_Pin_13;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOC, &gpio);
}

void spi3_init()
{
	spi3_gpio_init();

	SPI_InitTypeDef spi;
    spi.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    spi.SPI_DataSize = SPI_DataSize_8b;
    //spi.SPI_CPOL = SPI_CPOL_High;
    spi.SPI_CPOL = SPI_CPOL_Low;
   // spi.SPI_CPHA = SPI_CPHA_2Edge;
    spi.SPI_CPHA = SPI_CPHA_1Edge;
    spi.SPI_NSS = SPI_NSS_Soft;
    spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256; //todo nastoit daznis, kogda budet rabotat touch
    spi.SPI_FirstBit = SPI_FirstBit_MSB;
    spi.SPI_CRCPolynomial = 10;
    spi.SPI_Mode = SPI_Mode_Master;
    SPI_Init(SPI3, &spi);

    SPI_Cmd(SPI3, ENABLE);
}

void spi1_gpio_init()
{
	GPIO_InitTypeDef gpio;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

    // SPI:  PA5, PA6, PA7
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_5);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_5);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_5);

    gpio.GPIO_Mode = GPIO_Mode_AF;
    gpio.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOA, &gpio);

    //ns(cs) pin vrucnuju
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_Pin = GPIO_Pin_4;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOA, &gpio);
}

void spi1_init()
{
	spi1_gpio_init();

	SPI_InitTypeDef spi;
    spi.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    spi.SPI_DataSize = SPI_DataSize_8b;
    //spi.SPI_CPOL = SPI_CPOL_High;
    spi.SPI_CPOL = SPI_CPOL_Low;
   // spi.SPI_CPHA = SPI_CPHA_2Edge;
    spi.SPI_CPHA = SPI_CPHA_1Edge;
    spi.SPI_NSS = SPI_NSS_Soft;
    spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256; //todo nastoit daznis, kogda budet rabotat touch
    spi.SPI_FirstBit = SPI_FirstBit_MSB;
    spi.SPI_CRCPolynomial = 10;
    spi.SPI_Mode = SPI_Mode_Master;
    SPI_Init(SPI1, &spi);

    SPI_Cmd(SPI1, ENABLE);
}

void spi_sendByte(SPI_TypeDef* SPIx,uint8_t byteToSend)
{
    GPIOA->ODR &= ~GPIO_ODR_4; //todo ubrat
    GPIOC->ODR &= ~GPIO_ODR_13;

	uint16_t  timeout = TIMEOUT_TIME;
    while ((SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0))
    {
	timeout--;
    }

    //GPIOA->ODR |= GPIO_ODR_4; //todo mozet neuspevat otsilat
    SPI_SendData8(SPIx, byteToSend);
}

uint8_t spi_receiveByte( SPI_TypeDef* SPIx )
{
    GPIOA->ODR &= ~GPIO_ODR_4; //todo ubrat
    GPIOC->ODR &= ~GPIO_ODR_13;

    uint16_t timeout = TIMEOUT_TIME;
    while ((SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET) & (timeout != 0))
    {
	timeout--;
    }
    SPI_SendData8(SPIx, 0);

	timeout = TIMEOUT_TIME;
    while ((SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET) & (timeout != 0))
    {
	timeout--;
    }

    //GPIOA->ODR |= GPIO_ODR_4;
    return (uint8_t)SPI_ReceiveData8(SPIx);
}
