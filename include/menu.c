#include "menu.h"

void VALUES_IRQ_init( void )
{
	_SCREEN_TOUCH. _BUTTON_LEFT_irq = 0;
	_SCREEN_TOUCH. _BUTTON_RIGHT_irq = 0;
	_SCREEN_TOUCH. _BUTTON_UP_irq = 0;
	_SCREEN_TOUCH. _BUTTON_DOWN_irq = 0;

	_SCREEN_TOUCH. _SIGNAL_AREA_CENTER_UP_irq = 0;
	_SCREEN_TOUCH. _SIGNAL_AREA_CENTER_DOWN_irq = 0;
	_SCREEN_TOUCH. _SIGNAL_AREA_LEFT_irq = 0;
	_SCREEN_TOUCH. _SIGNAL_AREA_RIGHT_irq = 0;

	_SCREEN_TOUCH. _BUTTON_MENU_irq = 0;
}

void MENU_ON( uint8_t MenuChoose )
{

	 UG_DrawArc(_MENU_CIRCLE._X,_MENU_CIRCLE._Y,_MENU_CIRCLE._R,0b00110000,C_CORAL);

	 UG_DrawArc(480,86,\
	 	46,\
	 	0b00001100, C_CORAL);

	 UG_DrawArc(399,0,\
	 	_MENU_CIRCLE._R,\
	 	0b111000000, C_CORAL);

	 int tmp1=200,tmp2=271;
	 UG_DrawLine(_MENU_LINE_UP_HORIZ._X_L,\
	 	_MENU_LINE_UP_HORIZ._Y_L,\
	 	_MENU_LINE_UP_HORIZ._X_R,\
	 	_MENU_LINE_UP_HORIZ._Y_L,\
	 	C_CORAL);

	 UG_DrawLine(_MENU_LINE_LEFT_VERTIC._X_L,\
	 	_MENU_LINE_LEFT_VERTIC._Y_L,\
	 	_MENU_LINE_LEFT_VERTIC._X_R,\
	 	_MENU_LINE_LEFT_VERTIC._Y_R,\
	 	C_CORAL);

	 UG_DrawLine(_MENU_LINE_DOWN_HORIZ._X_L,\
		 _MENU_LINE_DOWN_HORIZ._Y_L,\
	 	_MENU_LINE_DOWN_HORIZ._X_R,\
	 	_MENU_LINE_DOWN_HORIZ._Y_R,\
	 	C_CORAL);

	 UG_DrawLine(_MENU_LINE_RIGHT_VERTIC._X_L,\
	 	_MENU_LINE_RIGHT_VERTIC._Y_L,\
	 	_MENU_LINE_RIGHT_VERTIC._X_R,\
	 	_MENU_LINE_RIGHT_VERTIC._Y_R,\
	 	C_CORAL);


	switch (MenuChoose){
		case 1 :
			 UG_FillFrame(_MENU_LINE_1._X_L,_MENU_LINE_1._Y_L,_MENU_LINE_1._X_R,_MENU_LINE_1._Y_R,MenuActive_color);
			 UG_FillFrame(_MENU_LINE_2._X_L,_MENU_LINE_2._Y_L,_MENU_LINE_2._X_R,_MENU_LINE_2._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_3._X_L,_MENU_LINE_3._Y_L,_MENU_LINE_3._X_R,_MENU_LINE_3._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_4._X_L,_MENU_LINE_4._Y_L,_MENU_LINE_4._X_R,_MENU_LINE_4._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_5._X_L,_MENU_LINE_5._Y_L,_MENU_LINE_5._X_R,_MENU_LINE_5._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_6._X_L,_MENU_LINE_6._Y_L,_MENU_LINE_6._X_R,_MENU_LINE_6._Y_R,MenuNonActive_color);

			break;
		case 2:
			 UG_FillFrame(_MENU_LINE_1._X_L,_MENU_LINE_1._Y_L,_MENU_LINE_1._X_R,_MENU_LINE_1._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_2._X_L,_MENU_LINE_2._Y_L,_MENU_LINE_2._X_R,_MENU_LINE_2._Y_R,MenuActive_color);
			 UG_FillFrame(_MENU_LINE_3._X_L,_MENU_LINE_3._Y_L,_MENU_LINE_3._X_R,_MENU_LINE_3._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_4._X_L,_MENU_LINE_4._Y_L,_MENU_LINE_4._X_R,_MENU_LINE_4._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_5._X_L,_MENU_LINE_5._Y_L,_MENU_LINE_5._X_R,_MENU_LINE_5._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_6._X_L,_MENU_LINE_6._Y_L,_MENU_LINE_6._X_R,_MENU_LINE_6._Y_R,MenuNonActive_color);
			 break;
		case 3 :
			 UG_FillFrame(_MENU_LINE_1._X_L,_MENU_LINE_1._Y_L,_MENU_LINE_1._X_R,_MENU_LINE_1._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_2._X_L,_MENU_LINE_2._Y_L,_MENU_LINE_2._X_R,_MENU_LINE_2._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_3._X_L,_MENU_LINE_3._Y_L,_MENU_LINE_3._X_R,_MENU_LINE_3._Y_R,MenuActive_color);
			 UG_FillFrame(_MENU_LINE_4._X_L,_MENU_LINE_4._Y_L,_MENU_LINE_4._X_R,_MENU_LINE_4._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_5._X_L,_MENU_LINE_5._Y_L,_MENU_LINE_5._X_R,_MENU_LINE_5._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_6._X_L,_MENU_LINE_6._Y_L,_MENU_LINE_6._X_R,_MENU_LINE_6._Y_R,MenuNonActive_color);
			 break;
		case 4 :
			 UG_FillFrame(_MENU_LINE_1._X_L,_MENU_LINE_1._Y_L,_MENU_LINE_1._X_R,_MENU_LINE_1._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_2._X_L,_MENU_LINE_2._Y_L,_MENU_LINE_2._X_R,_MENU_LINE_2._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_3._X_L,_MENU_LINE_3._Y_L,_MENU_LINE_3._X_R,_MENU_LINE_3._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_4._X_L,_MENU_LINE_4._Y_L,_MENU_LINE_4._X_R,_MENU_LINE_4._Y_R,MenuActive_color);
			 UG_FillFrame(_MENU_LINE_5._X_L,_MENU_LINE_5._Y_L,_MENU_LINE_5._X_R,_MENU_LINE_5._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_6._X_L,_MENU_LINE_6._Y_L,_MENU_LINE_6._X_R,_MENU_LINE_6._Y_R,MenuNonActive_color);
			 break;
		case 5 :
			 UG_FillFrame(_MENU_LINE_1._X_L,_MENU_LINE_1._Y_L,_MENU_LINE_1._X_R,_MENU_LINE_1._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_2._X_L,_MENU_LINE_2._Y_L,_MENU_LINE_2._X_R,_MENU_LINE_2._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_3._X_L,_MENU_LINE_3._Y_L,_MENU_LINE_3._X_R,_MENU_LINE_3._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_4._X_L,_MENU_LINE_4._Y_L,_MENU_LINE_4._X_R,_MENU_LINE_4._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_5._X_L,_MENU_LINE_5._Y_L,_MENU_LINE_5._X_R,_MENU_LINE_5._Y_R,MenuActive_color);
			 UG_FillFrame(_MENU_LINE_6._X_L,_MENU_LINE_6._Y_L,_MENU_LINE_6._X_R,_MENU_LINE_6._Y_R,MenuNonActive_color);
			break;

		case 6 :
			 UG_FillFrame(_MENU_LINE_1._X_L,_MENU_LINE_1._Y_L,_MENU_LINE_1._X_R,_MENU_LINE_1._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_2._X_L,_MENU_LINE_2._Y_L,_MENU_LINE_2._X_R,_MENU_LINE_2._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_3._X_L,_MENU_LINE_3._Y_L,_MENU_LINE_3._X_R,_MENU_LINE_3._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_4._X_L,_MENU_LINE_4._Y_L,_MENU_LINE_4._X_R,_MENU_LINE_4._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_5._X_L,_MENU_LINE_5._Y_L,_MENU_LINE_5._X_R,_MENU_LINE_5._Y_R,MenuNonActive_color);
			 UG_FillFrame(_MENU_LINE_6._X_L,_MENU_LINE_6._Y_L,_MENU_LINE_6._X_R,_MENU_LINE_6._Y_R,MenuActive_color);
			 break;
		default :
			break;
	}
}


MENU_HANDLER_VALUES_INIT()
{
	_MENU_MANAGER_HANDLER.VoltageTrigger = 1;
	_MENU_MANAGER_HANDLER.TruggerUpDown = 0; //1:rising 0:falling
	_MENU_MANAGER_HANDLER.DrawYesNo= 0; //0: no 1: yes
	_MENU_MANAGER_HANDLER.FreqTrigger = 0.5;
}


MENU_MANAGER_HANDLER( uint8_t MenuChoose , MenuManagerHandler LeftRight)
{
	 char menu_handler_buf[200];



	switch (MenuChoose){
		case 1 : //change trigger

			MENU_ON(MenuChoose);


			if (LeftRight == RIGHT){
				if(_MENU_MANAGER_HANDLER.VoltageTrigger < 3)
					_MENU_MANAGER_HANDLER.VoltageTrigger +=0.1;
			}
			else if(LeftRight == LEFT){
				if(_MENU_MANAGER_HANDLER.VoltageTrigger > 0.1)
					_MENU_MANAGER_HANDLER.VoltageTrigger -=0.1;
			}

				//otrisovka text
		 	UG_FontSelect(&FONT_8X12);
		 	UG_SetBackcolor ( MenuActive_color ) ;
		 	UG_SetForecolor ( MenuActiveText_color ) ;
		 	sprintf(menu_handler_buf,"Trigger level: %1.1fV",_MENU_MANAGER_HANDLER.VoltageTrigger);
			UG_PutString(_MENU_LINE_1._X_L+5,_MENU_LINE_1._Y_L+5,menu_handler_buf);

		 		/*
		 		 * cout text for other menu2
		 		 */
		 	UG_FontSelect(&FONT_8X12);
		 	UG_SetBackcolor ( MenuNonActive_color ) ;
		 	UG_SetForecolor ( MenuNonActiveText_color ) ;
		 	if(_MENU_MANAGER_HANDLER.TruggerUpDown == 0)
		 		sprintf(menu_handler_buf,"Trigger: DOWN");
		 	else
			 	sprintf(menu_handler_buf,"Trigger: UP");

			UG_PutString(_MENU_LINE_2._X_L+5,_MENU_LINE_2._Y_L+5,menu_handler_buf);

	 			/*
	 			 * cout text for other menu3
	 			 */
			if(_MENU_MANAGER_HANDLER.DrawYesNo == 1)
				 sprintf(menu_handler_buf,"Draw: Yes");
			else
				 sprintf(menu_handler_buf,"Draw: No");
			UG_PutString(_MENU_LINE_3._X_L+5,_MENU_LINE_3._Y_L+5,menu_handler_buf);

 				/*
 				 * cout text for other menu4
 				 */
			sprintf(menu_handler_buf,"Trigger Freq: %1.1fV",_MENU_MANAGER_HANDLER.FreqTrigger);
			UG_PutString(_MENU_LINE_4._X_L+5,_MENU_LINE_4._Y_L+5,menu_handler_buf);


			//default values //todo potom ispravit livehack (cveta meniaet v zavisimosti ot est ili nes sinhronizacii)
			UG_SetBackcolor ( Background_color ) ;
			UG_SetForecolor ( C_WHITE ) ;

			break;

		case 2:

			MENU_ON(MenuChoose);


			if(LeftRight == RIGHT || LeftRight == LEFT){
				if (_MENU_MANAGER_HANDLER.TruggerUpDown == 1)
					_MENU_MANAGER_HANDLER.TruggerUpDown = 0;
				else if (_MENU_MANAGER_HANDLER.TruggerUpDown == 0)
					_MENU_MANAGER_HANDLER.TruggerUpDown = 1;
			}

		 	UG_FontSelect(&FONT_8X12);
		 	UG_SetBackcolor ( MenuActive_color ) ;
		 	UG_SetForecolor ( MenuNonActive_color ) ;
		 	if(_MENU_MANAGER_HANDLER.TruggerUpDown == 0)
		 		sprintf(menu_handler_buf,"Trigger: DOWN");
		 	else
			 	sprintf(menu_handler_buf,"Trigger: UP");

			UG_PutString(_MENU_LINE_2._X_L+5,_MENU_LINE_2._Y_L+5,menu_handler_buf);



	 		/*
	 		 * cout text for other menu1
	 		 */
		 	UG_FontSelect(&FONT_8X12);
		 	UG_SetBackcolor ( MenuNonActive_color ) ;
		 	UG_SetForecolor ( MenuNonActiveText_color ) ;
		 	sprintf(menu_handler_buf,"Trigger level: %1.1fV",_MENU_MANAGER_HANDLER.VoltageTrigger);
			UG_PutString(_MENU_LINE_1._X_L+5,_MENU_LINE_1._Y_L+5,menu_handler_buf);

 			/*
 			 * cout text for other menu3
 			 */
			if(_MENU_MANAGER_HANDLER.DrawYesNo == 1)
				sprintf(menu_handler_buf,"Draw: Yes");
			else
			sprintf(menu_handler_buf,"Draw: No");
			UG_PutString(_MENU_LINE_3._X_L+5,_MENU_LINE_3._Y_L+5,menu_handler_buf);

			/*
			 * cout text for other menu4
			 */
			sprintf(menu_handler_buf,"Trigger Freq: %1.1fV",_MENU_MANAGER_HANDLER.FreqTrigger);
			UG_PutString(_MENU_LINE_4._X_L+5,_MENU_LINE_4._Y_L+5,menu_handler_buf);


			break;

		case 3:

			MENU_ON(MenuChoose);

			if(LeftRight == RIGHT || LeftRight == LEFT){
				if(_MENU_MANAGER_HANDLER.DrawYesNo == 0)
					_MENU_MANAGER_HANDLER.DrawYesNo = 1;
				else
					 _MENU_MANAGER_HANDLER.DrawYesNo = 0;
			}

			if(_MENU_MANAGER_HANDLER.DrawYesNo == 1)
				 sprintf(menu_handler_buf,"Draw: Yes");
			else
				 sprintf(menu_handler_buf,"Draw: No");

		 	UG_FontSelect(&FONT_8X12);
		 	UG_SetBackcolor ( C_TEAL ) ;
		 	UG_SetForecolor ( C_WHITE ) ;
			UG_PutString(_MENU_LINE_3._X_L+5,_MENU_LINE_3._Y_L+5,menu_handler_buf);



	 		/*
	 		 * cout text for other menu1
	 		 */
		 	UG_FontSelect(&FONT_8X12);
		 	UG_SetBackcolor ( MenuNonActive_color ) ;
		 	UG_SetForecolor ( MenuNonActiveText_color ) ;
		 	sprintf(menu_handler_buf,"Trigger level: %1.1fV",_MENU_MANAGER_HANDLER.VoltageTrigger);
			UG_PutString(_MENU_LINE_1._X_L+5,_MENU_LINE_1._Y_L+5,menu_handler_buf);


	 		/*
	 		 * cout text for other menu2
	 		 */
			if(_MENU_MANAGER_HANDLER.TruggerUpDown == 0)
				sprintf(menu_handler_buf,"Trigger: DOWN");
			else
				sprintf(menu_handler_buf,"Trigger: UP");

			UG_PutString(_MENU_LINE_2._X_L+5,_MENU_LINE_2._Y_L+5,menu_handler_buf);

			/*
			 * cout text for other menu4
			 */
			sprintf(menu_handler_buf,"Trigger Freq: %1.1fV",_MENU_MANAGER_HANDLER.FreqTrigger);
			UG_PutString(_MENU_LINE_4._X_L+5,_MENU_LINE_4._Y_L+5,menu_handler_buf);

			break;

		case 4:

			MENU_ON(MenuChoose);

//		 	UG_FontSelect(&FONT_8X12);
//		 	UG_SetBackcolor ( MenuActive_color ) ;
//		 	UG_SetForecolor ( MenuActiveText_color ) ;
//			sprintf(menu_handler_buf,"A/D/G: A!");
//			UG_PutString(_MENU_LINE_4._X_L+5,_MENU_LINE_4._Y_L+5,menu_handler_buf);

			if (LeftRight == RIGHT){
				if(_MENU_MANAGER_HANDLER.FreqTrigger < 3)
					_MENU_MANAGER_HANDLER.FreqTrigger +=0.1;
			}
			else if(LeftRight == LEFT){
				if(_MENU_MANAGER_HANDLER.FreqTrigger > 0.1)
					_MENU_MANAGER_HANDLER.FreqTrigger -=0.1;
			}


			UG_FontSelect(&FONT_8X12);
			UG_SetBackcolor ( MenuActive_color ) ;
			UG_SetForecolor ( MenuActiveText_color ) ;
		 	sprintf(menu_handler_buf,"Trigger Freq: %1.1fV",_MENU_MANAGER_HANDLER.FreqTrigger);
			UG_PutString(_MENU_LINE_4._X_L+5,_MENU_LINE_4._Y_L+5,menu_handler_buf);





	 		/*
	 		 * cout text for other menu1
	 		 */
		 	UG_FontSelect(&FONT_8X12);
		 	UG_SetBackcolor ( MenuNonActive_color ) ;
		 	UG_SetForecolor ( MenuNonActiveText_color ) ;
		 	sprintf(menu_handler_buf,"Trigger level: %1.1fV",_MENU_MANAGER_HANDLER.VoltageTrigger);
			UG_PutString(_MENU_LINE_1._X_L+5,_MENU_LINE_1._Y_L+5,menu_handler_buf);


	 		/*
	 		 * cout text for other menu2
	 		 */
			if(_MENU_MANAGER_HANDLER.TruggerUpDown == 0)
				sprintf(menu_handler_buf,"Trigger: DOWN");
			else
				sprintf(menu_handler_buf,"Trigger: UP");

			UG_PutString(_MENU_LINE_2._X_L+5,_MENU_LINE_2._Y_L+5,menu_handler_buf);

 			/*
 			 * cout text for other menu3
 			 */
			if(_MENU_MANAGER_HANDLER.DrawYesNo == 1)
				sprintf(menu_handler_buf,"Draw: Yes");
			else
			sprintf(menu_handler_buf,"Draw: No");
			UG_PutString(_MENU_LINE_3._X_L+5,_MENU_LINE_3._Y_L+5,menu_handler_buf);

			break;

		default :
			break;
	}


}




void MENU_OFF()
{

	 UG_DrawArc(_MENU_CIRCLE._X,_MENU_CIRCLE._Y,_MENU_CIRCLE._R,0b00110000,Background_color);

	 UG_DrawArc(480,86,\
	 	46,\
	 	0b00001100, Background_color);

	 UG_DrawArc(399,0,\
	 	_MENU_CIRCLE._R,\
	 	0b111000000, Background_color);

	 Draw_Signal_Area();
}


void MENU_ModeTextSettings(){
 	UG_FontSelect(&FONT_16X26);
 	UG_SetBackcolor ( Button_color ) ;
 	UG_SetForecolor ( C_BLACK ) ;
}

void UP_ParametersTextSettings(){
 	UG_FontSelect(&FONT_8X12);
 	UG_SetBackcolor ( Background_color ) ;
 	UG_SetForecolor ( C_WHITE ) ;
}

void MENUManagerTextActiveHandler(){
 	UG_FontSelect(&FONT_8X12);
 	UG_SetBackcolor ( C_TEAL ) ;
 	UG_SetForecolor ( C_WHITE ) ;
}

void MENUManagerTextNONActiveHandler(){
 	UG_FontSelect(&FONT_8X12);
 	UG_SetBackcolor ( C_WHITE ) ;
 	UG_SetForecolor ( C_BLACK ) ;
}

