#define TIMEOUT_TIME 1000

#ifndef SPI_H_
#define SPI_H_

/*for Touchscreen*/
void spi3_gpio_init();
void spi3_init();

/*for SDcard*/
void spi1_gpio_init();
void spi1_init();


void spi_sendByte(SPI_TypeDef* SPIx,uint8_t byteToSend);
uint8_t spi_receiveByte( SPI_TypeDef* SPIx );

#endif /* SPI_H_ */
