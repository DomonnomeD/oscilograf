#include "stm32f30x_conf.h"
struct _MENU_CIRCLE
{
	uint16_t	_X;
	uint16_t	_Y;
	uint16_t	_R;

}_MENU_CIRCLE;

struct _SIGNAL_OUTPUT
{
	uint16_t	_X_L;
	uint16_t	_X_R;
	uint16_t	_Y_L;
	uint16_t	_Y_R;
	uint16_t	_Step_x;
	uint16_t	_Step_y;
}_SIGNAL_OUTPUT;

struct _BUTTON_UP
{
	uint16_t	_X_L;
	uint16_t	_X_R;
	uint16_t	_Y_L;
	uint16_t	_Y_R;
	uint16_t	_X_Offset;
	uint16_t	_Y_Offset;

}_BUTTON_UP;

struct _BUTTON_DOWN
{
	uint16_t	_X_L;
	uint16_t	_X_R;
	uint16_t	_Y_L;
	uint16_t	_Y_R;
	uint16_t	_X_Offset;
	uint16_t	_Y_Offset;

}_BUTTON_DOWN;

struct _BUTTON_LEFT
{
	uint16_t	_X_L;
	uint16_t	_X_R;
	uint16_t	_Y_L;
	uint16_t	_Y_R;
	uint16_t	_X_Offset;
	uint16_t	_Y_Offset;

}_BUTTON_LEFT;

struct _BUTTON_RIGHT
{
	uint16_t	_X_L;
	uint16_t	_X_R;
	uint16_t	_Y_L;
	uint16_t	_Y_R;
	uint16_t	_X_Offset;
	uint16_t	_Y_Offset;

}_BUTTON_RIGHT;

struct _INFO_COUT_AREA
{
	uint16_t	_X;
	uint16_t	_Y;

}_INFO_COUT_AREA;

struct _SIGNAL_AREA
{
	uint16_t _Center_Up_X_L;
	uint16_t _Center_Down_X_L;
	uint16_t _Left_X_L;
	uint16_t _Right_X_L;

	uint16_t _Center_Up_Y_L;
	uint16_t _Center_Down_Y_L;
	uint16_t _Left_Y_L;
	uint16_t _Right_Y_L;

	uint16_t _Center_Up_X_R;
	uint16_t _Center_Down_X_R;
	uint16_t _Left_X_R;
	uint16_t _Right_X_R;

	uint16_t _Center_Up_Y_R;
	uint16_t _Center_Down_Y_R;
	uint16_t _Left_Y_R;
	uint16_t _Right_Y_R;
}_SIGNAL_AREA;


struct _MENU_LINE_UP_HORIZ
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_UP_HORIZ;

struct _MENU_LINE_RIGHT_VERTIC
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_RIGHT_VERTIC;

struct _MENU_LINE_LEFT_VERTIC
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_LEFT_VERTIC;

struct _MENU_LINE_DOWN_HORIZ
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_DOWN_HORIZ;

struct _MENU_LINE_1
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_1;

struct _MENU_LINE_2
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_2;

struct _MENU_LINE_3
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_3;

struct _MENU_LINE_4
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_4;

struct _MENU_LINE_5
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_5;

struct _MENU_LINE_6
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;

}_MENU_LINE_6;

//struct _MENU_LINE_7
//{
//	uint16_t	_X_L;
//	uint16_t	_Y_L;
//	uint16_t	_X_R;
//	uint16_t	_Y_R;
//
//}_MENU_LINE_7;

struct _DRAW_MODE_EXIT_ELEMENT
{
	uint16_t	_X_L;
	uint16_t	_Y_L;
	uint16_t	_X_R;
	uint16_t	_Y_R;
}_DRAW_MODE_EXIT_ELEMENT;
