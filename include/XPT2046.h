#define _start_bit		0b10000000
#define _y_pos		0b00010000
#define _z1_pos		0b00110000
#define _z2_pos		0b01000000
#define _x_pos		0b01010000
#define _mode_8		0b00001000
#define _mode_12		0b00000000
#define _single		0b00000100
#define _diff			0b00000000
#define _power_1		0b00000011 //Device is always powered. Reference is on and ADC is on.
#define _power_2		0b00000010 //Reference is on and ADC is off.
#define _power_3		0b00000001 //Reference is off and ADC is on
#define _power_4		0b00000000 // Power-Down Between Conversions. When each conversion is finished, the converter
																//enters a low-power mode. At the start of the next conversion, the device instantly
																//powers up to full power. There is no need for additional delays to ensure full operation,
																//and the very first conversion is valid. The Y− switch is on when in power-down.




#ifndef XPT2046_H_
#define XPT2046_H_

xpt2046_init();
uint16_t xpt2046_get_x();
uint16_t xpt2046_get_y();

#endif /* XPT2046_H_ */
