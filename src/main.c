 #include "main.h"
 #include "stm32f30x.h"

 int main(void){
 		/*clock*/
 	SystemInit();

 		/*gpio taktirovanie pokasto toko*/
 	GPIO_init();

 		/*for cout*/
 	trace_initialize();

 		/*ADC for batterry  PD10*/
	ADC3_init(1);
	ADC34_speed(1);
//	ADC_StartConversion(ADC3);
//	 trace_printf(" %d \n ",ADC3->DR);

 		/*ADC_DMA for chanel1  PA2*/
 	uint16_t SiZe = 10000; 	//(429-2)*44;
 	uint16_t buffer_chanel1_adc[SiZe];

 	int ADC1_init_value = 0;	//0..7 //todo v budusem uvelicit castotu (no dlia baigiamasisa stabilnuju sotavit)
 	int ADC12_speed_value = 11;	//0..11
 	ADC1_init(ADC1_init_value);
 	ADC12_speed(ADC12_speed_value);
 	//RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div1);
 	DMA1_ADC1_init(buffer_chanel1_adc,SiZe);

 		/*pwm pd14*/ // todo nastoiki sdelat ne v definah
 	PWM_TIM4_init();

 		/*external trigger  pd2*/
// 	TIM3_EXT_trigger();  //robit pocemuto ot navodki a ne ot tikanij v 3,5 volt

		/*Interrupt for button*/
	 EXTI_Config(EXTI_PortSourceGPIOC,EXTI_PinSource0,RISING);
	 EXTI_Config(EXTI_PortSourceGPIOC,EXTI_PinSource1,RISING);
	 EXTI_Config(EXTI_PortSourceGPIOC,EXTI_PinSource2,RISING);
	 EXTI_Config(EXTI_PortSourceGPIOD,EXTI_PinSource3,RISING);
	 EXTI_Config(EXTI_PortSourceGPIOD,EXTI_PinSource4,RISING);
	 	 /* interrupt for screen */
	EXTI_Config(EXTI_PortSourceGPIOC,EXTI_PinSource14,FALLING);

 		/*Touchscreen*/
 	spi3_init();
 	xpt2046_init();

 		/*menu*/
 	VALUES_IRQ_init();
 	MENU_HANDLER_VALUES_INIT();

 		/*screen*/
 	ScreenElementInit();
 	SSD_Init();
 	UG_GUI gui;

 	UG_Init(&gui,(void(*)(UG_S16,UG_S16,UG_COLOR))SSD_SetPixel,480,272);
 	UG_DriverRegister(DRIVER_FILL_FRAME, (void*)SSD_drawSquare);
 	UG_DriverEnable(DRIVER_FILL_FRAME);

 	DrawPrimaryView();
	Draw_Signal_Area();


 		/*that mode is on */
 	UG_FontSelect(&FONT_16X26);
 	UG_SetBackcolor ( Button_color ) ;
 	UG_SetForecolor ( C_BLACK ) ;

 	if(DigitAnalogGenerator == 2)
 		UG_PutString(455,1,"A");
 	else if(DigitAnalogGenerator == 1)
 		UG_PutString(455,1,"D");
 	else if(DigitAnalogGenerator == 3)
 		UG_PutString(455,1,"G");


	int trigger =0;	//uroven voltaza dlia sinhronizacii
	int ADCmaxValue=256-1;
	uint8_t ADCmaxVoltage = 3;
	#define DefaultIndexByTriggerValue 202
	int findindexbytrigger = DefaultIndexByTriggerValue;
	int StepFrequance=0;		//positive: narrow signal output
						//negative: wider signal output
						//0: no change
	int StepByTouchLeftRight = 0;
	int StepByTouchUpDown = 0;
	int StepByTouchAmplitude = 1;		//cant be zero, because used for mul/div
	uint8_t MenuActiveAreaPos = 1;
	uint8_t MaxMenuListNum = 4;

int duty=80;
int peremenaja=0;
int BatLevel = 0;

 	while(1)
 	{
 		//get baterry level in percent
 			/*0% - 2200
 			 *100% - 4231 (4231 need change)
 			 */
 		ADC_StartConversion(ADC3);
 		BatLevel = ADC3->DR * 100 / 4231;


 		//mux test
 		GPIOF->MODER = 0x55555555;
 		GPIOF->ODR |= (1<<9)|(1<<10);
 		//end mux

		if(duty == 560) peremenaja=0;
		else if(duty == 80) peremenaja=1;

		if(peremenaja == 0) duty-=40;
		else if(peremenaja == 1) duty+=40;

		TIM4->CCR3 = duty;



 			/*
 			 * wait until buffer fill up
 			 */
		 ADC_StartConversion(ADC1);

// 		while(_INT_IVENT._DMA1_Channel1 != 1){
// 			///trace_printf("zdu\n ");
// 		}
		 //250 bilo
		TIM7_delay_ms(250);	//without it DMA stuck
		 	 	 	 	 //important bez proverki ili bufer zapolnen zadrzka nedaet ego perezapisat
		 ADC_GetConversionValue(ADC1); 	//lifehack gor ADC stuck

			/*
			 * END
			 * wait until buffer fill up
			 */

		 trigger = _MENU_MANAGER_HANDLER.VoltageTrigger * ADCmaxValue / ADCmaxVoltage;
		 TriggerFront = _MENU_MANAGER_HANDLER.TruggerUpDown;

 			/*
 			 * find by trigger
 			 */
 		for (int i=_SIGNAL_OUTPUT._X_R; 	//start check after some values, for we can see before trigger values
 				i<SiZe/2-_SIGNAL_OUTPUT._X_R-2; 	//check only in halfbuffer
 				i++){
 			if(TriggerFront == 0 && findindexbytrigger == DefaultIndexByTriggerValue){ 	//falling
				if(buffer_chanel1_adc[i] < trigger && buffer_chanel1_adc[i] > trigger-10 &&
						buffer_chanel1_adc[i+1]<buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+2]<buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+3]<buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+4]<buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+5]<buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+6]<buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+7]<buffer_chanel1_adc[i]) {
						findindexbytrigger=i; 	//skakogo mesta vivodit
						UG_SetForecolor ( C_WHITE ) ;
				}
 			}
 			else if(TriggerFront == 1 && findindexbytrigger == DefaultIndexByTriggerValue){ 	//rasing
				if(buffer_chanel1_adc[i] > trigger && buffer_chanel1_adc[i] < trigger+10 &&
						buffer_chanel1_adc[i+1]>buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+2]>buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+3]>buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+4]>buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+5]>buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+6]>buffer_chanel1_adc[i] &&
						buffer_chanel1_adc[i+7]>buffer_chanel1_adc[i]) {
						findindexbytrigger=i; 	//skakogo mesta vivodit
						UG_SetForecolor ( C_WHITE ) ;
				}
 			}
 		}

 		if(findindexbytrigger == DefaultIndexByTriggerValue){
			for (int i=_SIGNAL_OUTPUT._X_R;i<SiZe/2-_SIGNAL_OUTPUT._X_R-2;i++){
				if(TriggerFront == 1  && findindexbytrigger == DefaultIndexByTriggerValue){ 	//rasing
					if (buffer_chanel1_adc[i] < trigger && buffer_chanel1_adc[i+1] > trigger)
					{
						UG_SetForecolor ( C_WHITE ) ;
						findindexbytrigger=i;
					}
				}

				if(TriggerFront == 0  && findindexbytrigger == DefaultIndexByTriggerValue){ 	//falling
					if (buffer_chanel1_adc[i] > trigger && buffer_chanel1_adc[i+1] < trigger)
					{
						UG_SetForecolor ( C_WHITE ) ;
						findindexbytrigger=i;
					}
				}
			}
 		}
			/*
			 * END
			 * find by trigger
			 */

 			/*
 			 *Voltage colcultaror
 			 */
 		int Min=255, Max=0;

 		for(int i=0; i<SiZe; i++)
 		{
 			if(Min > buffer_chanel1_adc[i]) Min=buffer_chanel1_adc[i];
 			if(Max < buffer_chanel1_adc[i]) Max=buffer_chanel1_adc[i];
 		}

 		float Voltage=0;

 		Voltage = (Max - Min) /2;
 		Voltage = (Voltage * 3) /255;

		//trace_printf("Min %d Max %d Voltage %2f \n",Min,Max,Voltage);
		TIM7_delay_ms(5);
			/*
			 *Voltage colculator
			 *End
			 */


			/*
			 * Frequnce colculator
			 */
 		uint16_t SameValuesCounter=0;
 		//uint16_t TriggerForFrequency =20; //0.2V
 		uint16_t TriggerForFrequency =_MENU_MANAGER_HANDLER.FreqTrigger * ADCmaxValue / ADCmaxVoltage; //0.5V  50
 		uint8_t CheckIfFound=0;
 		float FreqCoefficient[]={
 				0.0007,
 				0.0014,
 				0.0028,
 				0.0042,
 				0.0055833333333333,
 				0.007,
				0.0084615384615385,
 				0.0112,
				0.0224,
 				0.045,
 				0.09,
				0.1785714285714286};


 		int Freq = 0;
 		int Countbegin = 1;
 		if(findindexbytrigger != DefaultIndexByTriggerValue)
 			Countbegin =findindexbytrigger;

// 		for(int i=0; i<SiZe; i++ ){
// 			if(buffer_chanel1_adc[i] > TriggerFrequency-5 && buffer_chanel1_adc[i] < TriggerFrequency+5){
// 				if(CheckIfFound == 0){
// 					SameValuesCounter++;
// 					CheckIfFound=1;
// 				}
// 			}
// 			else
// 				CheckIfFound=0;
// 		}

// 		if(SameValuesCounter == 0)
 			for(int i=Countbegin; i<Countbegin+SiZe/2; i++ ){
				if(buffer_chanel1_adc[i] > TriggerForFrequency){
					if(CheckIfFound == 0){
						SameValuesCounter++;
						CheckIfFound=1;
					}
				}
				else
					CheckIfFound=0;
 			}

// 			ADC12_speed_value=0;

 			if( SameValuesCounter > 7 && SameValuesCounter < 2222 &&
 					ADC12_speed_value > 1)
 				Freq = SameValuesCounter / FreqCoefficient[ADC12_speed_value];
 			else if( SameValuesCounter > 7 && SameValuesCounter < 1300)
 				Freq = SameValuesCounter / FreqCoefficient[ADC12_speed_value];
 			else
 				Freq = 999999;

			/*
			 * END
			 * Frequnce colculator
			 */


 		if (findindexbytrigger == DefaultIndexByTriggerValue)
 			UG_SetForecolor ( C_RED ) ;
 		///else if(findindexbytrigger != DefaultIndexByTriggerValue){ //if sinchronizated (vivodit toko sinhronizirovanoe)

 			findindexbytrigger +=StepByTouchLeftRight;

				/*signal output*/
 			Draw_Signal_Area();

				/*cout signal*/
 			int dx=0;

 			int TMPstepfrequanceNARROW = 1;
 			int TMPstepfrequanceWIDE= 0;
			int TMPsignalforstep = 1;

 			if(StepFrequance > 0)
 				TMPstepfrequanceNARROW = StepFrequance;
 			else
 				TMPstepfrequanceWIDE= -StepFrequance;

				//for the cycle worked when TMPstepfrequanceWIDE = 1
			if(TMPstepfrequanceWIDE > 0)
				TMPsignalforstep = TMPstepfrequanceWIDE;

			for (int i=1;
					i<_SIGNAL_OUTPUT._X_R-TMPstepfrequanceWIDE;
					i+=TMPsignalforstep){

					//convert to int && flip axis for user
				int TMPy1 = ADCmaxValue-buffer_chanel1_adc[dx+findindexbytrigger]+1;
				int TMPy2 = ADCmaxValue-buffer_chanel1_adc[dx+TMPstepfrequanceNARROW+findindexbytrigger]+1;
				dx+=TMPstepfrequanceNARROW;

					//change amplitude
				if(StepByTouchAmplitude < 0){
					StepByTouchAmplitude*=-1;
					TMPy1/=StepByTouchAmplitude;
					TMPy2/=StepByTouchAmplitude;
					StepByTouchAmplitude*=-1;
				}
				else if(StepByTouchAmplitude > 0){
					TMPy1*=StepByTouchAmplitude;
					TMPy2*=StepByTouchAmplitude;
				}

					//move signal up and down (not amplitude)
				TMPy1 +=StepByTouchUpDown;
				TMPy2 +=StepByTouchUpDown;

					//if we changed amplitude, then need signal up/down (not amplitude)
					//for not signal go out out off screen
				if(StepByTouchAmplitude == 2){
					TMPy1-=255;
					TMPy2-=255;
				}
				else if(StepByTouchAmplitude == 3){
					TMPy1-=515;
					TMPy2-=515;
				}
				else if(StepByTouchAmplitude == 4){
					TMPy1-=760;
					TMPy2-=760;
				}
				else if(StepByTouchAmplitude == 5){
					TMPy1-=1000;
					TMPy2-=1000;
				}

				else if(StepByTouchAmplitude == -2){
					TMPy1+=120;
					TMPy2+=120;
				}
				else if(StepByTouchAmplitude == -3){
					TMPy1+=165;
					TMPy2+=165;
				}
				else if(StepByTouchAmplitude == -4){
					TMPy1+=190;
					TMPy2+=190;
				}
				else if(StepByTouchAmplitude == -5){
					TMPy1+=200;
					TMPy2+=200;
				}


					//check for NOT draw signal in NOT signal area
				if(TMPy1 >= _SIGNAL_OUTPUT._Y_R)		//check Down zone
					TMPy1 = _SIGNAL_OUTPUT._Y_R-1;
				else if(TMPy1 <= _SIGNAL_OUTPUT._Y_L)	//check Up zone
					TMPy1 = _SIGNAL_OUTPUT._Y_L+1;

				if(TMPy2 >= _SIGNAL_OUTPUT._Y_R)		//check Down zone
					TMPy2 = _SIGNAL_OUTPUT._Y_R-1;
				else if(TMPy2 <= _SIGNAL_OUTPUT._Y_L)	//check Up zone
					TMPy2 = _SIGNAL_OUTPUT._Y_L+1;

					//draw signal
				UG_DrawLine(i,TMPy1,i+TMPstepfrequanceWIDE,TMPy2, C_WHITE);

					//draw signal by pixel
				UG_DrawPixel(i-1,TMPy1,C_RED);
				UG_DrawPixel(i,TMPy1,C_RED);
				UG_DrawPixel(i-1,TMPy1,C_RED);

			}//for
		///}//if

			/*
			 * show trigger
			 */
 		UG_DrawLine(0,ADCmaxValue-trigger-3,2,ADCmaxValue-trigger,C_WHITE);
 		UG_DrawLine(0,ADCmaxValue-trigger+3,2,ADCmaxValue-trigger,C_WHITE);
 		UG_DrawLine(0,ADCmaxValue-trigger-3,0,ADCmaxValue-trigger+3,C_WHITE);
 		UG_DrawPixel(1,ADCmaxValue-trigger , C_WHITE);

 			/*
 			 * showfrequencycalculator level
 			 */
 		UG_DrawLine(0,ADCmaxValue-TriggerForFrequency-3,2,ADCmaxValue-TriggerForFrequency,C_DIM_GRAY);
 		UG_DrawLine(0,ADCmaxValue-TriggerForFrequency+3,2,ADCmaxValue-TriggerForFrequency,C_DIM_GRAY);
 		UG_DrawLine(0,ADCmaxValue-TriggerForFrequency-3,0,ADCmaxValue-TriggerForFrequency+3,C_DIM_GRAY);
 		UG_DrawPixel(1,ADCmaxValue-TriggerForFrequency , C_DIM_GRAY);



			/* cout information*/
 	 	UG_FontSelect(&FONT_8X12);
 	 	UG_SetBackcolor ( Background_color ) ;
 	 	//UG_SetForecolor change earler (red, nots sinchr, white, sinchr good)
// 		sprintf(buf,"StepFrequance: %d ADCspeed: %d Same: %d A: %d UD: %ds",  StepFrequance,\
// 											ADC12_speed_value,\
//											SameValuesCounter,\
//											StepByTouchAmplitude,\
//											StepByTouchUpDown);
// 		sprintf(buf,"%6dHz Step: %d ADCspeed: %d Count: %d",Freq,\
// 				StepFrequance,\
//				ADC12_speed_value,\
//				SameValuesCounter);

	 	if(_MENU_MANAGER_HANDLER.TruggerUpDown == 0)
 		sprintf(buf,"Bat:%d%% %6dHz %.2fV Trigger: DOWN",BatLevel,\
 				Freq,\
 				Voltage);
	 	else
	 	sprintf(buf,"Bat:%d%% %6dHz %.2fV Trigger: UP",BatLevel,\
	 			Freq,\
	 			Voltage);


		 UG_PutString(_INFO_COUT_AREA._X,_INFO_COUT_AREA._Y,"                                                     ");
		 UG_PutString(_INFO_COUT_AREA._X,_INFO_COUT_AREA._Y,buf);

		 	 //reinit ADC & ADC
		_INT_IVENT._DMA1_Channel1=0;
		DMA1_ADC1_init(buffer_chanel1_adc,SiZe);
		ADC12_speed(ADC12_speed_value);



		 	 /*
		 	  * change by TouchScreen&&buttons
		 	  */
		 if (_SCREEN_TOUCH. _SIGNAL_AREA_LEFT_irq >= 1)
			 if(StepByTouchLeftRight < 200 && (StepByTouchLeftRight + 20*_SCREEN_TOUCH. _SIGNAL_AREA_LEFT_irq) < 200) StepByTouchLeftRight +=20*_SCREEN_TOUCH. _SIGNAL_AREA_LEFT_irq;
		 if(_SCREEN_TOUCH. _SIGNAL_AREA_RIGHT_irq >= 1)
			 if(StepByTouchLeftRight > -200 && (StepByTouchLeftRight - 20*_SCREEN_TOUCH. _SIGNAL_AREA_RIGHT_irq) > -200) StepByTouchLeftRight -=20*_SCREEN_TOUCH. _SIGNAL_AREA_RIGHT_irq;
		 if(_SCREEN_TOUCH. _SIGNAL_AREA_CENTER_UP_irq >= 1)
			 if(StepByTouchUpDown < 2000) StepByTouchUpDown -=20*_SCREEN_TOUCH. _SIGNAL_AREA_CENTER_UP_irq;
		 if(_SCREEN_TOUCH. _SIGNAL_AREA_CENTER_DOWN_irq >= 1)
			 if(StepByTouchUpDown > -2000) StepByTouchUpDown +=20*_SCREEN_TOUCH. _SIGNAL_AREA_CENTER_DOWN_irq;




		 	 /*
		 	  * 	MENU
		 	  */
		 if( _SCREEN_TOUCH._BUTTON_MENU_irq >= 1){
			 VALUES_IRQ_init();
			 MENU_ON(MenuActiveAreaPos);

				//for menu settings
			while(_SCREEN_TOUCH._BUTTON_MENU_irq !=  1){

				//MENU_ON(MenuActiveAreaPos);

//todo optimizaiju sdelat vvide if... else if
//i ubrat if(_SCREEN_TOUCH._BUTTON_LEFT_irq == 0 && _SCREEN_TOUCH._BUTTON_RIGHT_irq == 0){
				if(_SCREEN_TOUCH. _BUTTON_DOWN_irq >= 1){
					if(MenuActiveAreaPos < MaxMenuListNum)	 MenuActiveAreaPos++;
//					MENU_ON(MenuActiveAreaPos);
					 _SCREEN_TOUCH. _BUTTON_DOWN_irq = 0;
				}
				if(_SCREEN_TOUCH. _BUTTON_UP_irq >= 1){
					 if(MenuActiveAreaPos > 1)  MenuActiveAreaPos--;
//					MENU_ON(MenuActiveAreaPos);
					 _SCREEN_TOUCH. _BUTTON_UP_irq = 0;
				}
				if(_SCREEN_TOUCH._BUTTON_LEFT_irq >= 1){
//					MENU_ON(MenuActiveAreaPos);
					MENU_MANAGER_HANDLER(MenuActiveAreaPos,LEFT);
					_SCREEN_TOUCH. _BUTTON_LEFT_irq = 0;
				}
				if(_SCREEN_TOUCH._BUTTON_RIGHT_irq >= 1){
//					MENU_ON(MenuActiveAreaPos);
					MENU_MANAGER_HANDLER(MenuActiveAreaPos,RIGHT);
					_SCREEN_TOUCH. _BUTTON_RIGHT_irq = 0;
				}
				if(_SCREEN_TOUCH._BUTTON_LEFT_irq == 0 && _SCREEN_TOUCH._BUTTON_RIGHT_irq == 0){
//					MENU_ON(MenuActiveAreaPos);
					MENU_MANAGER_HANDLER(MenuActiveAreaPos,NONE);
				}

				//wait until touch something
				while(_SCREEN_TOUCH. _BUTTON_DOWN_irq == 0 &&
						_SCREEN_TOUCH. _BUTTON_UP_irq == 0 &&
						_SCREEN_TOUCH._BUTTON_MENU_irq == 0 &&
						_SCREEN_TOUCH._BUTTON_LEFT_irq == 0 &&
						_SCREEN_TOUCH._BUTTON_RIGHT_irq == 0 );


			 	//TIM7_delay_ms(100); //kostil rezistorperepajat
			}

				//for go back into main loop
			MENU_OFF();

		 	UG_FontSelect(&FONT_16X26);
		 	UG_SetBackcolor ( Button_color ) ;
		 	UG_SetForecolor ( C_BLACK ) ;

		 	if(DigitAnalogGenerator == 2)
		 		UG_PutString(455,1,"A");
		 	else if(DigitAnalogGenerator == 1)
		 		UG_PutString(455,1,"D");
		 	else if(DigitAnalogGenerator == 3)
		 		UG_PutString(455,1,"G");

		 	UG_SetBackcolor ( Background_color ) ;
		}

		 	 /*
		 	  *	END menu
		 	  */

		 	 //signal change output parameter (amplitude)
		 if(_SCREEN_TOUCH. _BUTTON_UP_irq >=1){
		 	 if(StepByTouchAmplitude < 5) StepByTouchAmplitude +=1;
		 }
		 if(_SCREEN_TOUCH. _BUTTON_DOWN_irq >=1){
		 	 if(StepByTouchAmplitude > -5) StepByTouchAmplitude -=1;
		 }

//todo poniat kak rabotaet eta stuka (posmotret esli ee udalit, rabotaet li)
		 //pohodu sdes smotrit stob 0 nebilbi

		 if(StepByTouchAmplitude == 0 && _SCREEN_TOUCH. _BUTTON_UP_irq >=1)
			 StepByTouchAmplitude=1;
		 else if(StepByTouchAmplitude == 0 && _SCREEN_TOUCH. _BUTTON_DOWN_irq >=1)
			 StepByTouchAmplitude=-2;

		 	 //signal change output parameter (wider or narrower)
		 	 	 //testovo do 0 spustil, no nado testit
		 if(_SCREEN_TOUCH. _BUTTON_LEFT_irq >=1){	//bigger Hz wider
			 if (StepFrequance == 0)		//if can't change any more StepFrequance
				 if(ADC12_speed_value >= 0){
					ADC12_speed_value--;	//then change ADC12_speed_value
					ADC12_speed(ADC12_speed_value);
				 }
			 if(ADC12_speed_value == 0 || ADC12_speed_value == 11)	//if can't change any more ADC12_speed_value
				if(StepFrequance > -5) StepFrequance--;	//then change StepFrequance
		 }

		 if(_SCREEN_TOUCH. _BUTTON_RIGHT_irq >=1){	//less Hz narrow
			 if (StepFrequance == 0)		//if can't change any more StepFrequance
				 if(ADC12_speed_value < 11){
					ADC12_speed_value++;		//then change ADC12_speed_value
					ADC12_speed(ADC12_speed_value);
				 }
			 if(ADC12_speed_value == 0 || ADC12_speed_value == 11)	//if can't change any more ADC12_speed_value
				if(StepFrequance < 5) StepFrequance++;	//then change StepFrequance
		 }



		  		//dlia risovanija na touchscreen
		 if( _MENU_MANAGER_HANDLER.DrawYesNo == 1){

			UG_FillScreen(Background_color);

			UG_FillFrame(_DRAW_MODE_EXIT_ELEMENT._X_L,\
					_DRAW_MODE_EXIT_ELEMENT._Y_L,\
					_DRAW_MODE_EXIT_ELEMENT._X_R,\
					_DRAW_MODE_EXIT_ELEMENT._Y_R,\
					SignalBackground_color+10);


			NVIC_DisableIRQ(40);	//stob nemesalis pererivanie na touch

				//while not draw in area DrawYesNo
			  while(_MENU_MANAGER_HANDLER.DrawYesNo == 1){
				  uint16_t x_value,y_value;

				  x_value = xpt2046_get_LCD_x();
				  y_value = xpt2046_get_LCD_y();

				 UG_DrawPixel(x_value,y_value,C_WHITE);


					//for draw mode exit
				if(x_value > _DRAW_MODE_EXIT_ELEMENT._X_L && y_value > _DRAW_MODE_EXIT_ELEMENT._Y_L &&
						x_value < _DRAW_MODE_EXIT_ELEMENT._X_R && y_value < _DRAW_MODE_EXIT_ELEMENT._Y_R)
					_MENU_MANAGER_HANDLER.DrawYesNo = 0;

			  }

			NVIC_EnableIRQ(40);	//vrubaem rerivanie touch
			DrawPrimaryView();
		 }

		 VALUES_IRQ_init();
		 findindexbytrigger=DefaultIndexByTriggerValue;



 	}	//while
}












 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

 void DrawPrimaryView( void )
 {
		/*feel backround*/
	UG_FillScreen(Background_color);

		/*menu button*/
	for (int i=0; i<=_MENU_CIRCLE._R; i++)
		UG_DrawArc(_MENU_CIRCLE._X,_MENU_CIRCLE._Y,i,0b00110000,Menu_color);

		/*draw button (triangle LEFT)*/
	SSD_SetArea(_BUTTON_LEFT._X_Offset + _BUTTON_LEFT._X_L,\
			_BUTTON_LEFT._Y_Offset+ _BUTTON_LEFT._Y_L,\
			_BUTTON_LEFT._X_Offset + _BUTTON_LEFT._X_R,\
			_BUTTON_LEFT._Y_Offset + _BUTTON_LEFT._Y_R);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	uint16_t n=39;
	for(int i=0; i<(n-(n/2+1))/2;i++)
	  	for(int j=0; j<n; j++)
	   		SSD_WriteData(Background_color);

	for (int r = 1; r <= n/2+1; ++r) {
		for (int c = n/2+1-r; c >= 1; --c)
			SSD_WriteData(Background_color);
	for (int c = 1; c <= r*2-1; ++c)
		SSD_WriteData(Button_color);
	for (int c = n-(n/2)-r; c >= 1; --c)
		SSD_WriteData(Background_color);
	}

	for(int i=0; i<(n-(n/2+1))/2;i++)
	  	for(int j=0; j<n; j++)
	  		SSD_WriteData(Background_color);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

		/*draw button (triangle RIGHT)*/
	SSD_SetArea(_BUTTON_RIGHT._X_Offset + _BUTTON_RIGHT._X_L,\
			_BUTTON_RIGHT._Y_Offset+ _BUTTON_RIGHT._Y_L,\
			_BUTTON_RIGHT._X_Offset + _BUTTON_RIGHT._X_R,\
			_BUTTON_RIGHT._Y_Offset + _BUTTON_RIGHT._Y_R);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	     ///uint16_t n=39;
	for(int i=0; i<(n-(n/2+1))/2;i++)
	  	for(int j=0; j<n; j++)
	   		SSD_WriteData(Background_color);

	for (int r = n/2+1; r >= 1; --r) {
		for (int c = n/2+1-r; c >= 1; --c)
			SSD_WriteData(Background_color);
	for (int c = 1; c <= r*2-1; ++c)
		SSD_WriteData(Button_color);
	 for (int c = n-(n/2)-r; c >= 1; --c)
		 SSD_WriteData(Background_color);
	}

	for(int i=0; i<(n-(n/2+1))/2;i++)
	  	for(int j=0; j<n; j++)
	   		SSD_WriteData(Background_color);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);


		/*draw button (triangle DOWN)*/
	SSD_WriteCommand(SSD1963_SET_ADDRESS_MODE);	// flip x/y axys places
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0b00000011); 	// 0x00 set as default
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_SetArea(_BUTTON_DOWN._X_Offset + _BUTTON_DOWN._X_L,\
				_BUTTON_DOWN._Y_Offset+ _BUTTON_DOWN._Y_L,\
				_BUTTON_DOWN._X_Offset + _BUTTON_DOWN._X_R,\
				_BUTTON_DOWN._Y_Offset + _BUTTON_DOWN._Y_R);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	    // /uint16_t n=39;
	for(int i=0; i<(n-(n/2+1))/2;i++)
	  	for(int j=0; j<n; j++)
	   		SSD_WriteData(Background_color);

	for (int r = n/2+1; r >= 1; --r) {
		for (int c = n/2+1-r; c >= 1; --c)
			SSD_WriteData(Background_color);
	 for (int c = 1; c <= r*2-1; ++c)
		 SSD_WriteData(Button_color);
	 for (int c = n-(n/2)-r; c >= 1; --c)
		 SSD_WriteData(Background_color);
	}
	for(int i=0; i<(n-(n/2+1))/2;i++)
	  	for(int j=0; j<n; j++)
	   		SSD_WriteData(Background_color);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_WriteCommand(SSD1963_SET_ADDRESS_MODE); // flip x/y axys places
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0b00100011); //0x00 set as default
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

		/*draw button (triangle UP)*/
	SSD_WriteCommand(SSD1963_SET_ADDRESS_MODE); 	//flip x/y axys places
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0b00000011); 	//0x00 set as default
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_SetArea(_BUTTON_UP._X_Offset + _BUTTON_UP._X_L,\
			_BUTTON_UP._Y_Offset+ _BUTTON_UP._Y_L,\
			_BUTTON_UP._X_Offset + _BUTTON_UP._X_R,\
			_BUTTON_UP._Y_Offset + _BUTTON_UP._Y_R);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

		 ///uint16_t n=39;
	for(int i=0; i<(n-(n/2+1))/2;i++)
		for(int j=0; j<n; j++)
			SSD_WriteData(Background_color);

	for (int r = 1; r <= n/2+1; ++r) {
		for (int c = n/2+1-r; c >= 1; --c)
			SSD_WriteData(Background_color);
	for (int c = 1; c <= r*2-1; ++c)
		SSD_WriteData(Button_color);
	for (int c = n-(n/2)-r; c >= 1; --c)
		SSD_WriteData(Background_color);
	}
	for(int i=0; i<(n-(n/2+1))/2;i++)
	  	for(int j=0; j<n; j++)
	   		SSD_WriteData(Background_color);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_WriteCommand(SSD1963_SET_ADDRESS_MODE);	//flip x/y axys places
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0b00100011);  	//0x00 set as default
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

 }

 Draw_Signal_Area( void )
 {
		UG_FillFrame(_SIGNAL_OUTPUT._X_L,\
				_SIGNAL_OUTPUT._Y_L,\
				_SIGNAL_OUTPUT._X_R,\
				_SIGNAL_OUTPUT._Y_R,\
				SignalBackground_color);
		UG_DrawMesh_My(_SIGNAL_OUTPUT._X_L,\
				_SIGNAL_OUTPUT._Y_L,\
				_SIGNAL_OUTPUT._X_R,\
				_SIGNAL_OUTPUT._Y_R,\
				_SIGNAL_OUTPUT._Step_x,\
				_SIGNAL_OUTPUT._Step_y,\
					SignalMesh_color);
		UG_DrawFrame(_SIGNAL_OUTPUT._X_L,\
				_SIGNAL_OUTPUT._Y_L,\
				_SIGNAL_OUTPUT._X_R,\
				_SIGNAL_OUTPUT._Y_R,\
				SignalMesh_color);
 }

